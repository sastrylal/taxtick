<section style="width:85%;float:none;margin:10px auto;display:block;border:solid 1px #ddd;font-size: 14px;color: #333;padding: 10px 15px;">
    <p>Dear <?php echo!empty($customer_name) ? $customer_name : "Client"; ?>,</p>
    <p>Greetings from TaxTick!!!</p>
    <p>We have successfully served your request.</p>
    <p>If you are having any questions, call to our helpdesk +91 903-008-3030</p>
    <p><u><strong>Hope we have provided you the best service. If you like our service, please refer any of your colleagues, friends and family members.</strong></u></p>
    <br/>
    <p>
        Regards, <br/>
        TaxTick Team<br/>
        <a href="http://www.taxtick.in">www.taxtick.in</a><br/>
    </p>
</section>