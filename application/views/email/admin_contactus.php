<section style="width:85%;float:none;margin:10px auto;display:block;border:solid 1px #ddd;font-size: 14px;color: #333;padding: 10px 15px;">
    <p>Dear Admin,</p>            
    <p>The following e-mail ID and Phone number made an enquiry on Ask Us</p>
    <p><strong>Name:</strong> <?php echo !empty($name)?$name:""; ?></p>
    <p><strong>E-mail ID:</strong> <?php echo !empty($email)?$email:""; ?></p>
    <p><strong>Phone No:</strong> <?php echo !empty($mobile)?$mobile:""; ?></p>
    <p><strong>Message:</strong> <?php echo !empty($message)?$message:""; ?></p>
    <p><strong>Time:</strong> <?php echo date("d/m/Y H:i:s"); ?></p>
    <br/>
    <p>
        Regards, <br/>        
        <a href="http://www.taxtick.in">www.taxtick.in</a><br/>
    </p>
</section>