<!DOCTYPE html>
<html>
    <body>
        <section style="width:85%;float:none;margin:10px auto;display:block;border:solid 1px #ddd;font-size: 14px;color: #333;padding: 10px 15px;">
            <p>Dear <?php echo!empty($first_name) ? $first_name : "Client"; ?>,</p>
            <p>Thank you for registering with India’s only online Income Tax Assistant <a href="http://www.tacktick.in">www.tacktick.in</a>.</p>
            <p>If you haven’t registered, immediately report to +91 903-008-3030.</p>
            <p>Now, you are click away to the hassle-free Tax Filing by login <a href="http://www.tacktick.in">here</a></p>
            <p>
                <strong>Login E-Mail:</strong> <?php echo!empty($email) ? $email : ""; ?><br/>
                <strong>Password:</strong> <?php echo!empty($password) ? $password : ""; ?>
            </p>
            <p>If you are having any questions, call to our helpdesk +91 903-008-3030</p>
            <p>Happy Tax Filing!!!</p>
            <br/>
            <p>
                Regards, <br/>
                TaxTick Team<br/>
                <a href="http://www.taxtick.in">www.taxtick.in</a><br/>
            </p>
        </section>
    </body>
</html>