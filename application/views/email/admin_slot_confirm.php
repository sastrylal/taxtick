<section style="width:85%;float:none;margin:10px auto;display:block;border:solid 1px #ddd;font-size: 14px;color: #333;padding: 10px 15px;">
    <p>Dear Admin,</p>            
    <p>The following e-mail ID and Phone number booked their slot with taxtick.in</p>
    <p><strong>E-mail ID:</strong> <?php echo !empty($email)?$email:""; ?></p>
    <p><strong>Phone No:</strong> <?php echo !empty($mobile)?$mobile:""; ?></p>
    <p><strong>Type of Query:</strong> Slot booking</p>
    <p><strong>Slot:</strong> <?php echo !empty($slot_date)?$slot_date:""; ?></p>
    <br/>
    <p>
        Regards, <br/>        
        <a href="http://www.taxtick.in">www.taxtick.in</a><br/>
    </p>
</section>