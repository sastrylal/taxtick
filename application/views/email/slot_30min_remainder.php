<section style="width:85%;float:none;margin:10px auto;display:block;border:solid 1px #ddd;font-size: 14px;color: #333;padding: 10px 15px;">
    <p>Dear <?php echo!empty($customer_name) ? $customer_name : "Taxpayer"; ?>,</p>
    <p>Greetings from TaxTick!!!</p>
    <p>
        We confirm that your scheduled slot is arriving 30 min from now (<?php echo $slot_date_time->format("d-m-Y"); ?> at <?php echo $slot_date_time->format("G:iA"); ?>). You have selected “<?php echo!empty($sharing_software) ? $sharing_software : "TeamViewer"; ?>” for assisting you in your Tax Query.
        <?php if ($sharing_software != "Phone") { ?>
            Please make sure to download the “<?php echo!empty($sharing_software) ? $sharing_software : "TeamViewer"; ?>” application before the slot time.
        <?php } ?>
    </p>
    <p>If you are having any questions, call to our helpdesk +91 903-008-3030.</p>    
    <p>Thank you!!</p>
    <br/>
    <p>
        Regards, <br/>
        TaxTick Team<br/>
        <a href="http://www.taxtick.in">www.taxtick.in</a><br/>
    </p>
</section>