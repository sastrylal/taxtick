<section style="width:85%;float:none;margin:10px auto;display:block;border:solid 1px #ddd;font-size: 14px;color: #333;padding: 10px 15px;">
    <p>Dear <?php echo!empty($first_name) ? $first_name : "Client"; ?>,</p>            
    <p>Greetings from TaxTick!!!</p>
    <p>Please find the credentials:</p>
    <p><strong>Login E-Mail:</strong> <?php echo!empty($email) ? $email : ""; ?></p>
    <p><b>Click here to update Password :</b> <a href="http://taxtick.in/reset/?token=<?php echo!empty($token) ? $token : ""; ?>">Reset Password</a></p>
    <p>If you are having any questions, call to our helpdesk +91 903-008-3030.</p>
    <br/>
    <p>
        Regards, <br/>
        TaxTick Team<br/>
        <a href="http://www.taxtick.in">www.taxtick.in</a><br/>
    </p>
</section>