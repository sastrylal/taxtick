</div>
<script type="text/javascript" src="/js/wow.min.js"></script>
<script type="text/javascript">
    new WOW().init();
    jQuery(document).ready(function () {
        $('.mobile-icon').click(function () {
            $('.mobile').toggleClass('open');
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                $('.top-header').addClass("bg");
            } else {
                $('.top-header').removeClass("bg");
            }
        });
    });
</script>
</body>
</html>