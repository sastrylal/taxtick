Terms and conditions

In using this website you are deemed to have read and agreed to the following terms and

The following terminology applies to these Terms and Conditions, Privacy Statement and

Disclaimer Notice and any or all Agreements: &quot;Client&quot;, &quot;You&quot; and &quot;Your&quot; refers to you, the person

accessing TaxTick.in (&#39;website&#39;) and accepting the website&#39;s terms and conditions. &quot;Ourselves&quot;,

&quot;We&quot; and &quot;Us&quot;, refers to our website. &quot;Party&quot;, &quot;Parties&quot;, or &quot;Us&quot;, refers to both the Client and

ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and

consideration of payment necessary to undertake the process of our assistance to the Client in

the most appropriate manner, whether by formal meetings of a fixed duration, or any other

means, for the express purpose of meeting the Client&#39;s needs in respect of provision of the

website&#39;s stated services/products, in accordance with and subject to, prevailing Indian Law. Any

use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or

they, are taken as interchangeable and therefore as referring to the same.

We are committed to protecting your privacy. Authorized employees within the company on a

need-to- know basis only use any information collected from individual customers. We constantly

review our systems and data to ensure the best possible service to our customers.

Client records are regarded as confidential and therefore will not be divulged to any third party,

other than internal employees and the Indian Tax Departments and if legally required to do so to

the appropriate authorities. Clients have the right to request sight of, and copies of any and all

Client Records we keep, on the proviso that we are given reasonable notice of such a request.

Clients are requested to retain copies of any literature issued in relation to the provision of our

We will not sell, share, or rent your personal information to any third party or use your e-mail

address for unsolicited mail. Any emails sent by this Company will only be in connection with the

The information on this web site is provided on an &quot;as is&quot; basis. To the fullest extent permitted by

We excludes all representations and warranties relating to this website and its contents or which

is or may be provided by any affiliates or any other third party, including in relation to any

inaccuracies or omissions in this website; and excludes all liability for damages arising out of or

in connection with your use of this website. This includes, without limitation, direct loss, loss of

business or profits (whether or not the loss of such profits was foreseeable, arose in the normal

course of things or you have advised the website of the possibility of such potential loss), notices

by the Tax authorities, Scrutiny matters, appeals, damage caused to your computer, computer

software, systems and programs and the data thereon or any other direct or indirect,

All major Credit/Debit Cards and major bank Netbanking transfers are all acceptable methods of

payment. Our Terms of payment is full payment on an immediate basis. All services remain the

property of the Company until paid for in full. All transactions are verified with Verified by Visa

(VBV) or Mastercard Securecode or via a One Time Password. Chargebacks will not be

entertained in these circumstances. Only transactions which have failed due to a system error

will be refunded at the earliest. The charge will be reflected in your credit card or bank statement

Termination of Agreements and Refunds Policy

Both the Client and we have the right to terminate any Services Agreement for any reason,

including the ending of services that are already underway. No refunds shall be offered, where a

Service is deemed to have begun and is, for all intents and purposes, underway. Any monies that

have been paid to us which constitute payment in respect of the provision of unused Services

You may not create a link to any page of this website without our prior written consent. If you do

create a link to a page of this website you do so at your own risk and the exclusions and

limitations set out above will apply to your use of this website by linking to it.

Copyright and other relevant intellectual property rights exist on all text relating to the website&#39;s

Neither party shall be liable to the other for any failure to perform any obligation under any

Agreement which is due to an event beyond the control of such party including but not limited to

any act of God, terrorism, war, political insurgence, insurrection, riot, civil unrest, act of civil or

military authority, uprisings, earthquakes, floods or any other natural or manmade eventuality

outside of our control, which causes the termination of an agreement or contract entered into, nor

which could have been reasonably foreseen. Any Party affected by such event shall forthwith

inform the other Party of the same and shall use all reasonable endeavours to comply with the

terms and conditions of any Agreement contained herein.

Failure of either Party to insist upon strict performance of any provision of this or any Agreement

or the failure of either Party to exercise any right or remedy to which it, he or they are entitled

hereunder shall not constitute a waiver thereof and shall not cause a diminution of the obligations

under this or any Agreement. No waiver of any of the provisions of this or any Agreement shall

be effective unless it is expressly stated to be such and signed by both Parties.

The laws of India govern these terms and conditions. By accessing this website and using our

services, you consent to these terms and conditions and to the exclusive jurisdiction of the Indian

courts in all disputes arising out of such access. If any of these terms are deemed invalid or

unenforceable for any reason (including, but not limited to the exclusions and limitations set out

above), then the invalid or unenforceable provision will be severed from these terms and the

remaining terms will continue to apply. Failure of the website to enforce any of the provisions set

out in these Terms and Conditions and any Agreement, or failure to exercise any option to

terminate, shall not be construed as waiver of such provisions and shall not affect the validity of

these Terms and Conditions or of any Agreement or any part thereof, or the right thereafter to

enforce each and every provision. These Terms and Conditions shall not be amended, modified,

varied or supplemented except in writing and signed by duly authorised representatives of the

The website reserves the right to change these conditions from time to time as it sees fit and

your continued use of the site will signify your acceptance of any adjustment to these terms. If

there are any changes to our privacy policy, we will announce that these changes have been

made on our home page and on other key pages on our site. If there are any changes in how we

use our site customers&#39; Personally Identifiable Information, notification by e-mail or postal mail

will be made to those affected by this change. Any changes to our privacy policy will be posted

on our web site 30 days prior to these changes taking place. You are therefore advised to re-

These terms and conditions form part of the Agreement between the Client and us. Your

accessing of this website and/or undertaking of a booking or Agreement indicates your

understanding, agreement to and acceptance, of the Disclaimer Notice and the full Terms and

Conditions contained herein. Your statutory Consumer Rights are unaffected.