<div class="header-text  wow fadeInUp"  data-wow-duration="1s" data-wow-delay="1s">
    <div class="container">
        <h1>Forgot Password</h1>
    </div>
</div>
<div id="about-wrapper">
    <div class="container wow fadeInUp"  data-wow-duration="1s" data-wow-delay=".5s">
        <?php getMessage(); ?>
        <div class="form_container">            
            <form name="frm" id="frm" method="post">
                <input type="hidden" name="login" id="true" />
                <ul>
                    <li><input type="text" name="email" id="email" required="" placeholder="Email"></li>                   
                    <li><button type="submit" name="register" value="Submit">Reset Password</button></li>
                </ul>
                <span class="login_here"><a href="/login/">Login</a></span>
            </form>
        </div>
    </div>
</div>