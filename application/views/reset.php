<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<div class="header-text  wow fadeInUp"  data-wow-duration="1s" data-wow-delay="1s">
    <div class="container">
        <h1>Reset Password</h1>
    </div>
</div>
<div id="about-wrapper">
    <div class="container wow fadeInUp"  data-wow-duration="1s" data-wow-delay=".5s">
        <?php getMessage(); ?>
        <div class="form_container">            
            <form name="frm" id="frm" method="post">
                <input type="hidden" name="reset" id="reset" value="true" />
                <input type="hidden" name="token" id="token" value="<?php echo!empty($_GET['token']) ? $_GET['token'] : ""; ?>" />
                <ul>
                    <li><input type="password" name="password" id="password" class="required" placeholder="Password"></li>
                    <li><input type="password" name="confirm_password" id="confirm_password" class="required" placeholder="Confirm Password"></li>
                    <li><button type="submit" name="register" value="Submit">Reset Password</button></li>
                </ul>
                <span class="login_here"><a href="/login/">Login</a></span>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate({
            rules: {
                password: {
                    minlength: 5
                },
                confirm_password: {
                    minlength: 5,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: "Please enter password",
                },
                confirm_password: {
                    required: "Please enter confirm password",
                }
            }
        });
    });
</script>