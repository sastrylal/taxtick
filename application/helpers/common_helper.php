<?php
if (!function_exists("setError")) {
    function setError($message, $sub = ''){
        if(!empty($sub) && !empty($message)){
            $_SESSION['error'][$sub] = $message;
        }else if(!empty($message)){
            $_SESSION['error'] = $message;
        }
    }
}
if (!function_exists("setMessage")) {
    function setMessage($message, $sub = ''){
        if(!empty($sub) && !empty($message)){
            $_SESSION['message'][$sub] = $message;
        }else if(!empty($message)){
            $_SESSION['message'] = $message;
        }
    }
}
if (!function_exists("getMessage")) {
    function getMessage($sub = '') {
        if (!empty($sub) && !empty($_SESSION[$sub]['message'])) {
            echo '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $_SESSION[$sub]['message'] . '</div>';
            $_SESSION[$sub]['message'] = "";
        }// if end.
        if (empty($sub) && !empty($_SESSION['message'])) {
            echo '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $_SESSION['message'] . '</div>';
            $_SESSION['message'] = "";
        }// if end.
        if (!empty($sub) && !empty($_SESSION[$sub]['error'])) {
            echo '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $_SESSION[$sub]['error'] . '</div>';
            $_SESSION[$sub]['error'] = "";
        }// if end.
        if (empty($sub) && !empty($_SESSION['error'])) {
            echo '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $_SESSION['error'] . '</div>';
            $_SESSION['error'] = "";
        }// if end.
    }
}
if (!function_exists("generatePassword")) {
    function generatePassword($length = 8) {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        //'0123456789``-=~!@#$%^&*()_+,./<>?;:[]{}\|';
        $str = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++)
            $str .= $chars[rand(0, $max)];
        return $str;
    }
}
if (!function_exists("dateDB2SHOW")) {
    function dateDB2SHOW($db_date = "", $format = "d/m/Y", $display = "") {
        if (!empty($db_date) && $db_date != "0000-00-00" && $db_date != "0000-00-00 00:00:00") {
            $db_date = strtotime($db_date);
            return date($format, $db_date);
        }
        return $display;
    }
}
if (!function_exists("dateTimeDB2SHOW")) {
    function dateTimeDB2SHOW($db_date = "", $format = "", $display = "") {
        if (!empty($db_date) && $db_date != "0000-00-00" && $db_date != "0000-00-00 00:00:00") {
            $db_date = strtotime($db_date);
            if (!empty($format)) {
                return date($format, $db_date);
            } else {
                return date("d/m/Y h:i A", $db_date);
            }
        }
        return $display;
    }
}
if (!function_exists("dateForm2DB")) {
    function dateForm2DB($frm_date) {
        $frm_date = explode("/", $frm_date);
        if (!empty($frm_date[0]) && !empty($frm_date[1]) && !empty($frm_date[2])) {
            return $frm_date[2] . "-" . $frm_date[1] . "-" . $frm_date[0];
        } else {
            return "";
        }
    }
}
if (!function_exists("dateTimeForm2DB")) {
    function dateTimeForm2DB($frm_date) {
        $frm_date_time = explode(" ", $frm_date);
        $frm_date = explode("/", $frm_date_time[0]);
        $frm_time = explode(":", $frm_date_time[1]);
        if (!empty($frm_date[0]) && !empty($frm_date[1]) && !empty($frm_date[2])) {
            if (!isset($frm_time[0]))
                $frm_time[0] = "00";
            if (!isset($frm_time[1]))
                $frm_time[1] = "00";
            if (!isset($frm_time[2]))
                $frm_time[2] = "00";
            return $frm_date[2] . "-" . $frm_date[0] . "-" . $frm_date[1] . " " . $frm_time[0] . ":" . $frm_time[1] . ":" . $frm_time[2];
        } else {
            return "";
        }
    }
}
if (!function_exists("priceFormat")) {
    function priceFormat($price) {
        return number_format($price, 2);
    }
}
if (!function_exists("stdNameFormat")) {
    function stdNameFormat($str, $space = '-') {
        $str = strtolower($str);
        $str = str_replace("  ", " ", $str);
        $str = str_replace(" ", $space, $str);
        return $str;
    }
}
if (!function_exists("stdURLFormat")) {
    function stdURLFormat($str, $space = '-') {
        $str = strtolower($str);
        $str = preg_replace('/[^a-zA-Z0-9\-\ ]/i', '', $str);
        $str = str_replace("  ", " ", $str);
        $str = str_replace(" ", $space, $str);
        return $str;
    }
}
if (!function_exists("imgUpload")) {
    function imgUpload($field, $folder = '/data/', $file_name = '', $overwrite = true) {
        $allowed_types = 'gif|jpg|jpeg|png';
        if (!empty($_FILES[$field]['name'])) {
            if (!file_exists(DOC_ROOT_PATH . $folder)) {
                mkdir(DOC_ROOT_PATH . $folder, 0755, true);
            }
            $upload_path = DOC_ROOT_PATH . $folder;
            $file_info = pathinfo($_FILES[$field]['name']);
            $file_name = $file_name . "." . $file_info['extension'];
            $file_path = $upload_path . $file_name;
            if (@move_uploaded_file($_FILES[$field]["tmp_name"], $file_path)) {
                return $file_name;
            }
        }
        return false;
    }
}
if (!function_exists("docUpload")) {
    function docUpload($field, $folder = '/data/', $file_name = '', $overwrite = true) {
        $allowed_types = 'gif|jpg|jpeg|png|doc|DOC';
        if (!empty($_FILES[$field]['name'])) {            
            if (!file_exists(DOC_ROOT_PATH . $folder)) {
                mkdir(DOC_ROOT_PATH . $folder, 0755, true);
            }
            $upload_path = DOC_ROOT_PATH . $folder;
            $file_info = pathinfo($_FILES[$field]['name']);
            $file_name = $file_name . "." . $file_info['extension'];
            $file_path = $upload_path . $file_name;
            if (@move_uploaded_file($_FILES[$field]["tmp_name"], $file_path)) {
                return $file_name;
            }
        }
        return false;
    }
}
if (!function_exists("shortDesc")) {
    function shortDesc($str, $len = 300) {
        $str = substr($str, 0, $len);
        return $str;
    }
}
function curl_get_contents($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}
if (!function_exists("getIPInfo")) {
    function getIPInfo($ip = '') {
        if (empty($ip)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return json_decode(curl_get_contents("http://ipinfo.io/{$ip}/json"));
    }
}
if (!function_exists("monthNum2Str")) {
    function monthNum2Str($month) {
        $months = array(
            "1" => "January",
            "2" => "February",
            "3" => "March",
            "4" => "April",
            "5" => "May",
            "6" => "June",
            "7" => "July",
            "8" => "August",
            "9" => "September",
            "10" => "October",
            "11" => "November",
            "12" => "December",
        );
        if (isset($months[$month])) {
            return $months[$month];
        }
    }
}

function getBrowser() {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";
    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }
    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }
    return array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern
    );
}
if (!function_exists("apiRequest")) {
    function apiRequest($url, $request_method = "GET", $post_data = false, $headers = array(), $request_format = "", $response_format="")
    {
        $request_method = strtoupper($request_method);
        if ($post_data !== false && $request_method == "GET") {
            $url = $url."?".http_build_query($post_data);
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if ($post_data !== false && $request_method == "POST") {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            if($request_format == "json"){
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
            }else{
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
            }
        }
        if(!empty($request_method) && !in_array($request_method, ["GET", "POST"])){
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request_method);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if($request_format == "json"){
            $response = curl_exec($ch);
            return json_decode($response);
        }else{
            return curl_exec($ch);
        }
    }
}
?>