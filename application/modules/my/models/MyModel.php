<?php

class MyModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //DocDrive
    function addDoc($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_drive", $pdata);
        return $this->db->insert_id();
    }

    function updateDoc($doc_id, $pdata) {
        $this->db->where("doc_id", $doc_id);
        return $this->db->update("tbl_drive", $pdata);
    }

    function delDoc($doc_id) {
        $doc = $this->getDocById($doc_id);
        if (!empty($doc['doc_path']) && file_exists(DOC_ROOT_PATH . "/../doc_drive/" . $doc['doc_path'])) {
            @unlink(DOC_ROOT_PATH . "/../doc_drive/" . $doc['doc_path']);
        }
        $this->db->where("doc_id", $doc_id);
        return $this->db->delete("tbl_drive");
    }

    function getDocById($doc_id, $customer_id = "") {
        $this->db->select("m.*");
        $this->db->where("doc_id", $doc_id);
        if (!empty($customer_id)) {
            $this->db->where("customer_id", $customer_id);
        }
        $query = $this->db->get("tbl_drive m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchDocs($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, dt.doc_type_name");
            $this->db->join("tbl_doc_types dt", "m.doc_type=dt.doc_type_id", "left");
        }
        if (!empty($s['key'])) {
            $this->db->where("m.doc_name LIKE '%" . $s['key'] . "%'");
        }
        if (!empty($s['customer_id'])) {
            $this->db->where("m.customer_id", $s['customer_id']);
        }
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->order_by("m.doc_id DESC");
        $query = $this->db->get("tbl_drive m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    //Askus
    function addAsk($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_askus", $pdata);
        return $this->db->insert_id();
    }

    function updateAsk($ask_id, $pdata) {
        $this->db->where("ask_id", $ask_id);
        return $this->db->update("tbl_askus", $pdata);
    }

    function delAsk($ask_id) {
        $this->db->where("ask_id", $ask_id);
        return $this->db->delete("tbl_askus");
    }

    function getAskById($ask_id) {
        $this->db->select("m.*, c.first_name AS customer_name, c.email, c.mobile");
        $this->db->join("tbl_customers c", "m.customer_id=c.customer_id");
        $this->db->where("ask_id", $ask_id);
        $query = $this->db->get("tbl_askus m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchAskus($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*");
        }
        if (!empty($s['key'])) {
            $this->db->join("tbl_ask_messages msg", "m.ask_id=msg.ask_id");
            $this->db->where("m.title LIKE '%" . $s['key'] . "%' OR  msg.message LIKE '%" . $s['key'] . "%' ");
        }
        if (!empty($s['customer_id'])) {
            $this->db->where("customer_id", $s['customer_id']);
        }
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->order_by("m.ask_id DESC");
        $query = $this->db->get("tbl_askus m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function addAskMessage($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_ask_messages", $pdata);
        return $this->db->insert_id();
    }

    function updateAskMessage($msg_id, $pdata) {
        $this->db->where("msg_id", $msg_id);
        return $this->db->update("tbl_ask_messages", $pdata);
    }

    function delAskMessage($msg_id) {
        $this->db->where("msg_id", $msg_id);
        return $this->db->delete("tbl_ask_messages");
    }

    function getAskMessages($ask_id) {
        $this->db->where("ask_id", $ask_id);
        $this->db->order_by("m.msg_id DESC");
        $query = $this->db->get("tbl_ask_messages m");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    //Slots
    function getAvailableSlotDates() {
        $available_slot_dates = [];
        $i = 0;
        $nowDate = new DateTime('now');
        while ($i++ <= 30) {
            $available_slot_dates[] = $nowDate->format("d/m/Y");
            $nowDate->modify("+1 days");
        }
        return $available_slot_dates;
    }

    function getAvailableSlotTimes($slot_date) {
        $available_slot_times = [];
        $nowDate = new DateTime(date("Y-m-d") . " 10:00:00");
        $staffCnt = $this->getAvailableStaffCnt($slot_date);
        $booked_slots = $this->getBookedSlots($slot_date);
        if (!empty($slot_date)) {
            $slot_date = new DateTime($slot_date);
            if ($slot_date->format("Y-m-d") == date("Y-m-d")) {
                $nowDate = new DateTime();
                $nowDate->modify("+1 60min");
                if ($nowDate->format("i") > 30) {
                    $nowDate->setTime($nowDate->format("H"), 30, 0);
                } else {
                    $nowDate->setTime($nowDate->format("H"), 0, 0);
                }
            }
        }
        $endDate = new DateTime(date("Y-m-d") . " 18:00:00");
        while ($nowDate <= $endDate) {
            $slotKey = $nowDate->format("H:i:s");
            $slotVal = $nowDate->format("h:i A");            
            if ($staffCnt > 0) {
                if (empty($booked_slots) || !isset($booked_slots[$slotKey])) {
                    $available_slot_times[$slotKey] = $slotVal;
                } else if (isset($booked_slots[$slotKey]) && $booked_slots[$slotKey] < $staffCnt) {
                    $available_slot_times[$slotKey] = $slotVal;
                }
            }
            $nowDate->modify("+1 30min");
        }
        return $available_slot_times;
    }

    function getAvailableStaffCnt($slot_date) {
        $slot_date = new DateTime($slot_date);
        $this->db->select("m.schedule_id");
        $this->db->where("m.schedule_day", $slot_date->format("N"));
        $this->db->where("m.is_working", "Yes");
        $this->db->where("m.user_id NOT IN (SELECT user_id FROM tbl_leaves WHERE leave_date = '".$slot_date->format("Y-m-d")."')");
        $query = $this->db->get("tbl_schedule_settings m");        
        return $query->num_rows();
    }

    function getBookedSlots($slot_date) {
        $rows = [];
        $slot_date = new DateTime($slot_date);
        $this->db->select("m.slot_date, COUNT(m.slot_date) AS slot_cnt");
        $this->db->where("DATE(m.slot_date)", $slot_date->format("Y-m-d"));
        $this->db->where("m.payment_status", "Paid");
        $this->db->group_by("m.slot_date");
        $this->db->order_by("m.slot_date");
        $query = $this->db->get("tbl_slots m");
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $slot_date_time = $row['slot_date'];
                $slot_date_time = explode(" ", $slot_date_time);
                $rows[$slot_date_time[1]] = $row['slot_cnt'];
            }
        }
        return $rows;
    }

    function getPromoByCode($promo_code) {
        $this->db->select("m.*");
        $this->db->where("promo_code", $promo_code);
        $this->db->where("is_active", "1");
        $query = $this->db->get("tbl_promos m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function addSlot($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_slots", $pdata);
        return $this->db->insert_id();
    }

    function updateSlot($slot_id, $pdata) {
        $this->db->where("slot_id", $slot_id);
        return $this->db->update("tbl_slots", $pdata);
    }

    function delSlot($slot_id) {
        $this->db->where("slot_id", $slot_id);
        return $this->db->delete("tbl_slots");
    }

    function getSlotById($slot_id) {
        $this->db->select("m.*");
        $this->db->where("slot_id", $slot_id);
        $query = $this->db->get("tbl_slots m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchSlots($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*");
        }
        if (!empty($s['customer_id'])) {
            $this->db->where("m.customer_id", $s['customer_id']);
        }
        if (!empty($s['payment_status'])) {
            $this->db->where("m.payment_status", $s['payment_status']);
        }
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->order_by("m.slot_id DESC");
        $query = $this->db->get("tbl_slots m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function savePaymentLog($pdata) {
        $this->db->insert("tbl_payment_logs", $pdata);
        return $this->db->insert_id();
    }

    //Faqs
    function searchFaqs($s, $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*", false);
        }
        if (isset($s['is_active'])) {
            $this->db->where("m.is_active", $s['is_active']);
        }
        $this->db->order_by("m.faq_id ASC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("tbl_faqs m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function getDocTypeList() {
        $this->db->select("m.*");
        $this->db->where("is_active", "1");
        $query = $this->db->get("tbl_doc_types m");
        if ($query->num_rows() > 0) {
            $rows = [];
            foreach ($query->result_array() as $row) {
                $rows[$row['doc_type_id']] = $row['doc_type_name'];
            }
            return $rows;
        }
        return false;
    }

    //Customers
    function addCustomer($pdata) {
        $this->db->insert("tbl_customers", $pdata);
        return $this->db->insert_id();
    }

    function updateCustomerByPk($customer_id, $pdata) {
        $this->db->where("customer_id", $customer_id);
        return $this->db->update("tbl_customers", $pdata);
    }

    function updateCustomer($pdata, $customer_id) {
        $this->db->where("customer_id", $customer_id);
        return $this->db->update("tbl_customers", $pdata);
    }

    function delCustomer($customer_id) {
        $this->db->where("customer_id", $customer_id);
        return $this->db->delete("tbl_customers");
    }

    function getCustomerById($customer_id) {
        $this->db->select("m.*");
        $this->db->where("customer_id", $customer_id);
        $query = $this->db->get("tbl_customers m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

}

?>