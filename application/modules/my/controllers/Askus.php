<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Askus extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("MyModel", "myModel", TRUE);
        $this->_customer_check();
    }

    public function index() {
        if (!empty($_POST['ASKUS'])) {
            $pdata = [];
            $pdata['customer_id'] = $_SESSION['CUSTOMER_ID'];
            $pdata['title'] = !empty($_POST['title']) ? $_POST['title'] : "";
            $pdata['status'] = "Awaiting Reply";
            $ask_id = $this->myModel->addAsk($pdata);
            if (!empty($ask_id)) {
                $msg_id = $this->myModel->addAskMessage([
                    "ask_id" => $ask_id,
                    "message_by" => "Customer",
                    "message" => (!empty($_POST['message']) ? $_POST['message'] : "")
                ]);
                $attach = docUpload("attach", "/data/ask/", "doc" . $msg_id);
                if (!empty($attach)) {
                    $this->myModel->updateAskMessage($msg_id, [
                        "attach_name" => $_FILES["attach"]["name"],
                        "attach_path" => $attach
                    ]);
                }
            }
            $ask = $this->myModel->getAskById($ask_id);
            $data = $ask;
            $data['to'] = 'info@taxtick.in';
            $data['subject'] = "Enquiry ". $ask['customer_name'];
            $this->sendEmail("email/admin_askus", $data);
            setMessage("Your message has been sent.");
            redirect(base_url() . "my/askus/");
        } else if (!empty($_POST['ASKMSG'])) {
            $msg_id = $this->myModel->addAskMessage([
                "ask_id" => (!empty($_POST['ask_id']) ? $_POST['ask_id'] : ""),
                "message_by" => "Customer",
                "message" => (!empty($_POST['message']) ? $_POST['message'] : "")
            ]);
            $attach = docUpload("attach", "/data/ask/", "doc" . $msg_id);
            if (!empty($attach)) {
                $this->myModel->updateAskMessage($msg_id, [
                    "attach_name" => $_FILES["attach"]["name"],
                    "attach_path" => $attach
                ]);
            }
            setMessage("Your message has been sent.");
            redirect(base_url() . "my/askus/");
        }
        $data = [];
        $search_data = [];        
        if (!empty($_GET['key'])) {
            $search_data["key"] = $_GET['key'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->hidden_data = ["customer_id" => $_SESSION['CUSTOMER_ID']];
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/my/askus/?';
        $this->pagenavi->process($this->myModel, 'searchAskus');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['items'] = $this->pagenavi->items;
        $data['sno'] = $this->pagenavi->sno;
        $data['desc_sno'] = $this->pagenavi->desc_sno;
        $this->_template("askus/index", $data);
    }

    public function ask_messages() {
        $data = [];
        if (!empty($_GET['ask_id'])) {
            $data['ask'] = $this->myModel->getAskById($_GET['ask_id']);
            $data['messages'] = $this->myModel->getAskMessages($_GET['ask_id']);
            $this->load->view("askus/ask_messages", $data);
        }
    }

}

?>