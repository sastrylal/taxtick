<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DocDrive extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("MyModel", "myModel", TRUE);
        $this->_customer_check();
    }

    public function index() {
        if (!empty($_GET['act']) && $_GET['act'] == "del" && !empty($_GET['doc_id'])) {
            $this->myModel->delDoc($_GET['doc_id']);
            setMessage("Your document has been deleted.");
            redirect(base_url() . "my/docDrive/");
        }
        if (!empty($_POST['DOC_UPDATE']) && !empty($_POST['doc_id'])) {
            $pdata = [];
            $pdata['financial_year'] = !empty($_POST['financial_year']) ? $_POST['financial_year'] : "";
            $pdata['doc_type'] = !empty($_POST['doc_type']) ? $_POST['doc_type'] : "";
            $pdata['doc_name'] = !empty($_POST['doc_name']) ? $_POST['doc_name'] : "";
            $pdata['doc_password'] = !empty($_POST['doc_password']) ? $_POST['doc_password'] : "";
            $doc_id = $this->myModel->updateDoc($_POST['doc_id'], $pdata);
            setMessage("Your document has been updated.");
            redirect(base_url() . "my/docDrive/");
        }
        $data = [];
        $search_data = [];
        if (!empty($_GET['key'])) {
            $search_data['key'] = $_GET['key'];
        }
        $search_data['customer_id'] = $_SESSION['CUSTOMER_ID'];
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/my/docDrive/?';
        $this->pagenavi->process($this->myModel, 'searchDocs');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['docs'] = $this->pagenavi->items;
        $data['sno'] = $this->pagenavi->sno;
        $data['desc_sno'] = $this->pagenavi->desc_sno;
        $this->_template("docdrive/index", $data);
    }

    public function addDoc() {
        if (!empty($_POST['doc_name'])) {
            $pdata = [];
            $pdata['customer_id'] = $_SESSION['CUSTOMER_ID'];
            $pdata['financial_year'] = !empty($_POST['financial_year']) ? $_POST['financial_year'] : "";
            $pdata['doc_type'] = !empty($_POST['doc_type']) ? $_POST['doc_type'] : "";
            $pdata['doc_name'] = !empty($_POST['doc_name']) ? $_POST['doc_name'] : "";
            $pdata['doc_password'] = !empty($_POST['doc_password']) ? $_POST['doc_password'] : "";
            $doc_id = $this->myModel->addDoc($pdata);
            $doc_path = docUpload("doc_path", "/../doc_drive/" . $pdata['customer_id'] . "/", "doc" . $doc_id);
            if (!empty($doc_path)) {
                $this->myModel->updateDoc($doc_id, [
                    "doc_path" => $doc_path
                ]);
            }
            setMessage("Your document has been uploaded.");
            redirect(base_url() . "my/docDrive/");
        }
        $data = [];
        $data['doc_types'] = $this->myModel->getDocTypeList();
        $this->_template("docdrive/adddoc", $data);
    }

    public function download($doc_id = "") {
        if (!empty($doc_id)) {
            $doc = $this->myModel->getDocById($doc_id, $_SESSION['CUSTOMER_ID']);
            if (!empty($doc['doc_id'])) {
                $this->load->helper('download');
                $doc_folder = dirname(DOC_ROOT_PATH) . "/doc_drive/" . $doc['customer_id'] . "/";
                if (file_exists($doc_folder . $doc['doc_path'])) {
                    $doc_data = file_get_contents($doc_folder . $doc['doc_path']);
                    $doc_path_ext = pathinfo($doc['doc_path'], PATHINFO_EXTENSION);
                    $doc_name = $doc['doc_name'].".".$doc_path_ext;
                    force_download($doc_name, $doc_data);
                    exit;
                }
            }
        }
    }

    public function edit($doc_id = "") {
        $data = [];
        $doc = $this->myModel->getDocById($doc_id, $_SESSION['CUSTOMER_ID']);
        if (!empty($doc['doc_id'])) {
            $data['doc'] = $doc;
            $data['doc_types'] = $this->myModel->getDocTypeList();
            $this->load->view("docdrive/form", $data);
        }
    }

}

?>