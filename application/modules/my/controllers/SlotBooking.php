<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SlotBooking extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("MyModel", "myModel", TRUE);
        $this->_customer_check();
    }

    public function index() {        
        $data = [];
        if (!empty($_POST['slot_date']) && !empty($_POST['slot_time'])) {
            $slot = [];
            $slot['customer_id'] = $_SESSION['CUSTOMER_ID'];
            $slot['customer_name'] = !empty($_POST['customer_name']) ? $_POST['customer_name'] : "";
            $slot['purpose'] = !empty($_POST['purpose']) ? $_POST['purpose'] : "";
            $slot['email'] = !empty($_POST['email']) ? $_POST['email'] : "";
            $slot['mobile'] = !empty($_POST['mobile']) ? $_POST['mobile'] : "";
            $slot['slot_date'] = dateForm2DB($_POST['slot_date']) . " " . $_POST['slot_time'];
            $slot['sharing_software'] = !empty($_POST['sharing_software']) ? $_POST['sharing_software'] : "";
            $slot['promo_code'] = !empty($_POST['promo_code']) ? $_POST['promo_code'] : "";
            $slot['amount'] = !empty($_POST['amount']) ? $_POST['amount'] : "";
            $slot_id = $this->myModel->addSlot($slot);
            redirect(base_url() . "my/payment_request/?slot_id=" . $slot_id);
        }
        $customer = $this->myModel->getCustomerById($_SESSION['CUSTOMER_ID']);
        $slot = [];
        $slot['customer_name'] = !empty($customer['first_name']) ? $customer['first_name'] : "";
        $slot['mobile'] = !empty($customer['mobile']) ? $customer['mobile'] : "";
        $slot['email'] = !empty($customer['email']) ? $customer['email'] : "";
        $data['available_slot_dates'] = $this->myModel->getAvailableSlotDates();
        $data['slot'] = $slot;
        $this->_template("slotbooking/booking", $data);
    }

    public function step1() {
        $data = [];
        $this->_template("slotbooking/step1", $data);
    }

    public function step2() {
        $data = [];
        $this->_template("slotbooking/step2", $data);
    }

    public function booking() {
        
    }

    public function available_slot_times() {
        $slot_times = [];
        if (!empty($_POST['slot_date'])) {
            $slot_date = dateForm2DB($_POST['slot_date']);
            $slot_times = $this->myModel->getAvailableSlotTimes($slot_date);
        }
        header('Content-Type: application/json');
        echo json_encode($slot_times);
    }

    public function promo_check() {
        $response = [];
        $response['slot_cost'] = SLOT_COST;
        if (!empty($_POST['promo_code'])) {
            $promo = $this->myModel->getPromoByCode($_POST['promo_code']);
            if (!empty($promo['promo_id'])) {
                if($promo['promo_type'] == "Percentage"){
                    $response['slot_cost'] = SLOT_COST - ((SLOT_COST * $promo['discount'])/100);
                }else{
                    $response['slot_cost'] = SLOT_COST - $promo['discount'];
                }
                $response['slot_cost'] = number_format($response['slot_cost'], 2, ".", "");
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

}

?>