<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class My extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("MyModel", "myModel", TRUE);
        $this->_customer_check();
    }

    public function index() {
        redirect(base_url() . "my/slotBooking/");
        $data = [];
        $this->_template("dashboard", $data);
    }

    public function profile() {
        $data = [];
        if (!empty($_POST['PROFILE'])) {
            $pdata = [];
            $pdata['first_name'] = !empty($_POST['first_name']) ? $_POST['first_name'] : "";
            $pdata['last_name'] = !empty($_POST['last_name']) ? $_POST['last_name'] : "";
            $pdata['mobile'] = !empty($_POST['mobile']) ? $_POST['mobile'] : "";
            $this->myModel->updateCustomerByPk($_SESSION['CUSTOMER_ID'], $pdata);
            setMessage("Your profile has been updated");
            redirect(base_url() . "my/profile/");
        } else if (!empty($_POST['PWD']) && !empty($_POST['password'])) {
            $this->myModel->updateCustomerByPk($_SESSION['CUSTOMER_ID'], [
                "password" => $_POST['password']
            ]);
            setMessage("Your password has been updated");
            redirect(base_url() . "my/profile/");
        }
        $data['customer'] = $this->myModel->getCustomerById($_SESSION['CUSTOMER_ID']);
        $this->_template("profile", $data);
    }

    public function logout() {
        $_SESSION = [];
        redirect(base_url() . "login/");
    }

    public function payment_request() {
        if ($this->input->get("slot_id") !== null) {
            $data = array();
            $slot_id = $this->input->get("slot_id");
            $slot = $this->myModel->getSlotById($slot_id);
            if (!empty($slot['promo_code']) && $slot['amount'] == "0.00") {
                $_SESSION['SLOT_ID'] = $slot_id;
                $this->myModel->updateSlot($slot_id, array('payment_status' => 'Paid', 'slot_status' => 'In Process'));
                //setMessage("You had successfully paid. Our team will contact you soon!");
                redirect(base_url() . "my/payment_confirm/");
            }
            $data['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $data['customer'] = $this->myModel->getCustomerById($_SESSION['CUSTOMER_ID']);
            $data['slot'] = $slot;
            $this->myModel->updateSlot($slot_id, array('txn_id' => $data['txnid']));
            $this->load->view("payment_request", $data);
        } else {
            redirect(base_url() . "my/");
        }
    }

    public function payment_success() {
        if (!empty($_POST['status']) && $_POST['status'] == "success") {
            $this->_payment_log();
            $slot_id = !empty($_POST['udf1']) ? $_POST['udf1'] : "";
            $this->myModel->updateSlot($slot_id, array('payment_status' => 'Paid', 'slot_status' => 'In Process'));
            $_SESSION['SLOT_ID'] = $slot_id;
            $this->sendEmail("email/slot_confirm", $slot);
            //setMessage("You had successfully paid. Our team will contact you soon!");
            redirect(base_url() . "my/payment_confirm/");
        }
    }

    public function payment_confirm() {
        if (!empty($_SESSION['SLOT_ID'])) {
            $data = [];
            $slot = $this->myModel->getSlotById($_SESSION['SLOT_ID']);
            $data['slot'] = $slot;
            $slot_date_time = new DateTime($slot['slot_date']);
            $slot['slot_date_time'] = $slot_date_time;
            $slot['to'] = $slot['email'];
            $slot['subject'] = "Slot Confirmation - " . $slot_date_time->format("d-m-Y") . " at " . $slot_date_time->format("G:iA") . " (www.taxtick.in)";
            $this->sendEmail("email/slot_confirm", $slot);           
            
            $slot['to'] = 'info@taxtick.in';
            $slot['subject'] = $slot['customer_name'] . " - New Slot Booking";
            $this->sendEmail("email/admin_slot_confirm", $slot);
            
            $this->sendSMS($slot['mobile'], "Dear ".$slot['customer_name'].", we confirm that your slot has been blocked for ".$slot_date_time->format("d-m-Y")." at " . $slot_date_time->format("G:iA") . " – www.taxtick.in");
            unset($_SESSION['SLOT_ID']);
            $this->_template("payment_confirm", $data);
        } else {
            redirect(base_url() . "my/");
        }
    }

    public function payment_fail() {
        $this->_payment_log();
        setError("Your Payment is not done.");
        redirect(base_url() . "my/history/");
    }

    public function payment_cancel() {
        $this->_payment_log();
        redirect(base_url() . "my/history/");
    }

    private function _payment_log() {
        $log = array();
        $log['tnx_id'] = !empty($_POST['txnid']) ? $_POST['txnid'] : "";
        $log['slot_id'] = !empty($_POST['udf1']) ? $_POST['udf1'] : "";
        $log['response'] = serialize($_POST);
        $this->myModel->savePaymentLog($log);
    }

    public function payment_thanku() {
        $data = array();
        if (isset($_SESSION['CMP_ORDER_ID']) && !empty($_SESSION['CMP_ORDER_ID'])) {
            $data['order'] = $this->myModel->getOrderById($_SESSION['CMP_ORDER_ID']);
            $data['order_items'] = $this->myModel->getOrderItems($_SESSION['CMP_ORDER_ID']);
            //$data['customer'] = $this->myModel->getCustomerById($order['customer_id']);
            unset($_SESSION['CMP_ORDER_ID']);
        }
        $this->_template('payment_thanku', $data);
    }

}

?>