<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>slot payment Request</title>
    </head>
    <body>
        <?php
        //Test Server details
        //$MERCHANT_KEY = "IjyFDW0M";
        //$SALT = "TQSElqbMiQ";        
        //$PAYU_BASE_URL = "https://test.payu.in"; // End point - change to https://secure.payu.in for LIVE mode

        $MERCHANT_KEY = "IjyFDW0M";
        $SALT = "TQSElqbMiQ";
        $PAYU_BASE_URL = "https://secure.payu.in";

        $action = '';
        $formError = 0;
        //$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $hash = '';
        // Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        $action = $PAYU_BASE_URL . '/_payment';

        $posted['key'] = $MERCHANT_KEY;
        $posted['txnid'] = $txnid;
        $posted['amount'] = !empty($slot['amount']) ? $slot['amount'] : "0";
        $posted['surl'] = base_url() . "my/payment_success/";
        $posted['furl'] = base_url() . "my/payment_fail/";
        $posted['curl'] = base_url() . "my/payment_cancel/";
        $posted['service_provider'] = "payu_paisa";
        $posted['productinfo'] = "Slot Booking";

        $posted['firstname'] = !empty($slot['customer_name']) ? $slot['customer_name'] : "";
        $posted['lastname'] = !empty($slot['last_name']) ? $slot['last_name'] : "";
        $posted['address1'] = !empty($slot['address1']) ? $slot['address1'] : "";
        $posted['address2'] = !empty($slot['address2']) ? $slot['address2'] : "";
        $posted['city'] = !empty($slot['city']) ? $slot['city'] : "";
        $posted['state'] = !empty($slot['state']) ? $slot['state'] : "";
        $posted['country'] = !empty($slot['country']) ? $slot['country'] : "";
        $posted['zipcode'] = !empty($slot['zip_code']) ? $slot['zip_code'] : "";
        $posted['email'] = !empty($slot['email']) ? $slot['email'] : "";
        $posted['phone'] = !empty($slot['mobile']) ? $slot['mobile'] : "";
        $posted['udf1'] = !empty($slot['slot_id']) ? $slot['slot_id'] : "";

        if (empty($posted['hash']) && sizeof($posted) > 0) {
            if (
                    empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['phone']) || empty($posted['productinfo']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])
            ) {
                //print_r($posted);
                $formError = 1;
            } else {
                //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution     fee","value":"1500","isRequired":"false"}]'));
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach ($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }
                $hash_string .= $SALT;
                $hash = strtolower(hash('sha512', $hash_string));
                $action = $PAYU_BASE_URL . '/_payment';
                $posted['hash'] = $hash;
            }
        } elseif (!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
        }
        ?>
        <form method="post" name="redirect" action="<?php echo $action; ?>"> 
            <?php
            foreach ($posted as $key => $val) {
                echo '<input type="hidden" name="' . $key . '" value="' . $val . '" />' . "\n";
            }
            ?>
        </form>
        <script language='javascript'>document.redirect.submit();</script>
    </body>
</html>