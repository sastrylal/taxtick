<div class="user-title">
    <input type="hidden" name="ask_id" value="<?php echo!empty($ask['ask_id']) ? $ask['ask_id'] : ""; ?>" />
    <?php if (!empty($messages)) { ?>
        <?php foreach ($messages as $message) { ?>
            <p class="<?php echo ($message['message_by'] == "Customer") ? 'query' : 'answer'; ?>">
                <?php echo!empty($message['message']) ? nl2br($message['message']) : ""; ?>
                <?php if (!empty($message['attach_path'])) { ?>
                <br><span><a href="/data/ask/<?php echo $message['attach_path']; ?>" target="_blank"><?php echo $message['attach_name']; ?></a></span>
                <?php } ?>
            </p>
        <?php } ?>
    <?php } ?>
    <div class="form-group">
        <label for="message">Any Further Question?</label>
        <textarea name="message" id="message" class="" placeholder="Message"></textarea>
    </div>
    <div class="form-group">
        <label for="attach">Attachment</label>
        <input type="file" name="attach" id="attach" />
    </div>
</div>