<div id="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-typical box-typical-padding addproduct-fromgrup">
                    <h5 class="m-t-lg with-border m-t-0">Ask Us</h5>
                    <div class="form-group row ">
                        <form method="get">                            
                            <div class="col-sm-4">
                                <p class="form-control-static">
                                    <input type="text" placeholder="" id="key" name="key" value="<?php echo!empty($_GET['key']) ? $_GET['key'] : ""; ?>" class="form-control" />
                                </p>
                            </div>                            
                            <div class="col-sm-2">
                                <p class="form-control-static">
                                    <button type="submit" class="btn btn-green">Search</button>
                                    <a href="/my/askus/" class="btn btn-green">Reset</a>
                                </p>
                            </div>                            
                        </form>
                        <button type="button" class="btn btn-green query-btn pull-right" data-toggle="modal" data-target="#querymodel">New Query</button>                        
                    </div>                    
                    <?php getMessage(); ?>
                    <div class="form-group row ">
                        <div class="col-sm-12">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Date</th>
                                        <th>Query Title</th>
                                        <th width="160px;">Status</th>
                                    </tr>
                                    <?php if (!empty($items)) { ?>
                                        <?php foreach ($items as $item) {
                                            ?>
                                            <tr>
                                                <td><?php echo $desc_sno--; ?></td>
                                                <td><?php echo!empty($item['created_on']) ? dateTimeDB2SHOW($item['created_on']) : ""; ?></td>
                                                <td><a href="/my/askus/ask_messages/?ask_id=<?php echo $item['ask_id']; ?>" data-remote="false" data-toggle="modal" data-target="#answermodel"><?php echo $item['title']; ?></a></td>
                                                <td <?php echo ($item['status'] == "Replied")?'style="background-color: #00ff00;"':'style="background-color: yellow;"'; ?>><?php echo $item['status']; ?></td>
                                            </tr>
                                        <?php } ?> 
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="4">No queries found.</td>
                                        </tr>
                                    <?php } ?>                                    
                                </tbody>
                            </table>
                            <?php echo !empty($PAGING)?$PAGING:""; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

<div id="querymodel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form name="frm" id="frm" method="post" action="" role="form" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Query</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="ASKUS" id="ASKUS" value="true" />
                    <div class="form-group">
                        <label for="title">Query Title</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Title" />
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="" placeholder="Message"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="attach">Attachment</label>
                        <input type="file" name="attach" id="attach" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="answermodel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form name="frm" id="frm" method="post" action="" role="form" enctype="multipart/form-data">
            <input type="hidden" name="ASKMSG" value="true" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="answer_title"></h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $("#answermodel").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $("#answer_title").html(link.html());
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>