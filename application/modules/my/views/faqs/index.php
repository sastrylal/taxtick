<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<div id="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <?php if(!empty($faqs)){ $flag = true; ?>
                <div class="box-typical box-typical-padding addproduct-fromgrup faq-collpc">
                    <h5 class="m-t-lg with-border m-t-0">FAQ's</h5>
                    <button class="expand">Expand All</button>
                    <div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php foreach ($faqs as $faq) { ?>
                            <div class="panel">
                                <div class="panel-heading" role="tab" id="heading<?php echo $faq['faq_id']; ?>">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq['faq_id']; ?>" aria-expanded="true" aria-controls="collapseOne">
                                            <?php echo $faq['title']; ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $faq['faq_id']; ?>" class="panel-collapse collapse <?php echo ($flag === true)? "in":""; ?>" role="tabpanel" aria-labelledby="heading<?php echo $faq['faq_id']; ?>">
                                    <div class="panel-body">
                                        <?php echo $faq['description']; ?>
                                    </div>
                                </div>
                            </div>
                            <!-- end of panel -->
                            <?php $flag = false; ?>
                        <?php } ?>                        
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>