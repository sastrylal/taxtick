<?php
if (!empty($slot['slot_date'])) {
    $slot_date = new DateTime($slot['slot_date']);
}
?>
<div id="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 center-block">
                <div class="content center-block">
                    <p>Dear <?php echo!empty($slot['customer_name']) ? $slot['customer_name'] : ""; ?>,</p>                    
                    <p>
                        We confirm your payment. <br />
                        Confirmed slot details: <br />
                        Date	-	<?php echo!empty($slot_date) ? $slot_date->format("d/m/Y") : ""; ?> <br />
                        Time	-	<?php echo $slot_date->format("g:i A");
$slot_date->modify("+20 min"); ?> to <?php echo $slot_date->format("g:i A"); ?>  <br />
                        Screensharing software	-	<?php echo!empty($slot['sharing_software']) ? $slot['sharing_software'] . ' <i class="' . strtolower($slot['sharing_software']) . '"></i>' : ""; ?> <br />
                    </p>

                    <p>Our team will send you a remainder SMS 30 min before the slot time.</p>

                    <p>
                        Regards: <br/>
                        Team TaxTick, <br/>
                        9030083030. <br/>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>