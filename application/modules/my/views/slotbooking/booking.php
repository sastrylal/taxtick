<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<div id="page-content">
    <div class="container-fluid">
        <?php getMessage(); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box-typical box-typical-padding addproduct-fromgrup">
                    <form id="frm" name="frm" method="post" action="">
                        <h5 class="m-t-lg with-border m-t-0">Schedule Your Slot</h5>                    
                        <div class="form-group row ">
                            <div class="col-sm-6">
                                <p class="form-control-static">
                                    <label class="form-label semibold" for="customer_name">Name <span class="red">*</span></label>
                                    <input type="text" placeholder="Name" id="customer_name" name="customer_name" class="form-control required" value="<?php echo!empty($slot['customer_name']) ? $slot['customer_name'] : ""; ?>" />
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <p class="form-control-static">
                                    <label class="form-label semibold" for="email">Email <span class="red">*</span></label>
                                    <input type="email" placeholder="Email" id="email" name="email" class="form-control required" value="<?php echo!empty($slot['email']) ? $slot['email'] : ""; ?>"  />
                                </p>
                            </div>                            
                        </div>
                        <div class="form-group row ">                            
                            <div class="col-sm-6">
                                <p class="form-control-static">
                                    <label class="form-label semibold" for="mobile">Mobile Number <span class="red">*</span></label>
                                    <input type="text" placeholder="Mobile" id="mobile" name="mobile" class="form-control required" value="<?php echo!empty($slot['mobile']) ? $slot['mobile'] : ""; ?>" />
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <p class="form-control-static">
                                    <label class="form-label semibold" for="purpose">Purpose of Scheduling <span class="red">*</span></label>
                                    <select name="purpose" id="purpose" class="form-control required">
                                        <option>Income Tax Return e-Filing</option>
                                        <option>Notice Received from Income Tax Department</option>
                                        <option>Investment Advice</option>
                                        <option>Salary Structure Advice</option>
                                        <option>Tax Planning</option>
                                        <option>Selection of Insurance Policy/Loan/MF</option>
                                        <option>Issue with CIBIL Rating</option>
                                        <option>Others</option>
                                    </select>                                    
                                </p>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <div class="col-sm-6">
                                <div class="form-control-static">
                                    <div class="form-control-static">
                                        <label class="form-label semibold" for="slot_date">Slot Date <span class="red">*</span></label>
                                        <input type="text" name="slot_date" id="slot_date" class="form-control required" placeholder="Select date">
                                    </div>
                                </div>                                
                                <div class="form-control-static col-sm-12 pad0 timeslot">
                                    <div class="form-control-static">
                                        <label class="form-label semibold" for="slot_time" id="slot_time_label" style="display: none;">Slot Time <span class="red">*</span></label>
                                        <input type="hidden" name="slot_time" id="slot_time" class="" value=""/>
                                        <ul class="slot-timings" id="slot_timings">

                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-12 pad0">
                                    <div class="form-control-static">
                                        <label class="form-label semibold" for="sharing_software">Screen Sharing Software <span class="red">*</span></label>
                                        <div class="radio">
                                            <label><input type="radio" name="sharing_software" value="Skype" checked="true"><i class="skype"></i></label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="sharing_software" value="Gtalk"><i class="gtalk"></i></label>
                                        </div>                                        
                                        <div class="radio">
                                            <label><input type="radio" name="sharing_software" value="Teamviewer"><i class="teamviewer"></i></label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="sharing_software" value="Ammyy"><i class="ammyy"></i></label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="sharing_software" value="Phone"><i class="phone"></i></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                 <p class="form-control-static">
                                    <label class="form-label semibold" for="promo_code">Promo code</label>
                                    <input type="text" placeholder="Promo code" id="promo_code" name="promo_code" class="form-control" value="<?php echo!empty($slot['promo_code']) ? $slot['promo_code'] : ""; ?>" />
                                </p>                               
                            </div>
                            <div class="col-sm-6 ">
                                <p class="form-control-static">
                                    <label class="form-label semibold" for="amount" >Amount Payable (&#x20B9)</label>
                                    <input type="text" value="<?php echo SLOT_COST; ?>" id="amount" name="amount" class="form-control required" readonly="true" />
                                </p>
                            </div>
                        </div>                    
                        <div class="form-group row mgb0">
                            <div class="col-sm-12">
                                </br>
                                <h5 class="with-border"></h5>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-green">Proceed to Payment</button> 
                                    <a href="/my/" class="btn btn-secondary">Cancel</a>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#slot_timings").on("click", "a", function (event) {
            event.preventDefault();
            $("#slot_timings a").removeClass('active');
            $(this).addClass('active');
            $("#slot_time").val($(this).data('slot_time'));
        });
        $('#slot_date').datepicker({
            format: "dd/mm/yyyy",
            startDate: '0',
            endDate: '+30d'
        }).on('changeDate', function () {
            $.ajax({
                url: "/my/slotBooking/available_slot_times/",
                type: "POST",
                dataType: 'json',
                data: {"slot_date": $('#slot_date').val()},
                success: function (data) {
                    $("#slot_time_label").show();
                    $("#slot_time").val("");
                    $("#slot_time").addClass('required');
                    $("#slot_timings").html("");
                    $.each(data, function (i, item) {
                        $('#slot_timings').append('<li> <a href="#" data-slot_time="' + i + '"> ' + item + ' </a></li>');
                    });
                }
            });
        });
        $("#frm").validate({
            ignore: "",
            rules: {
                mobile: {required: true, digits: true, minlength: 10, maxlength: 10}
            },
            messages: {
                customer_name: {required: "Please enter customer name"},
                email: {required: "Please enter email address"},
                mobile: {required: "Please enter mobile number", minlength: "Please enter valid mobile", maxlength: "Please enter valid mobile"},
                slot_date: {required: "Please select date"},
                slot_time: {required: "Please select time slot"}
            }
        });
        $("#promo_code").on('change', function () {
            var promo_code = $("#promo_code").val();
            if (promo_code.length >= 3 || promo_code.length == 0) {
                $.ajax({
                    url: "/my/slotBooking/promo_check/",
                    type: "POST",
                    dataType: 'json',
                    data: {"promo_code": $('#promo_code').val()},
                    success: function (data) {
                        $("#amount").val(data.slot_cost);
                    }
                });
            }
        });
    });
</script>