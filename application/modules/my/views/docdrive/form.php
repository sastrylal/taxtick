<input type="hidden" name="doc_id" id="doc_id" value="<?php echo!empty($doc['doc_id']) ? $doc['doc_id'] : ""; ?>" />
<div class="form-group row ">
    <div class="col-sm-6">
        <p class="form-control-static">
            <label class="form-label semibold" for="financial_year">Financial Year</label>
            <select class="select2" name="financial_year" id="financial_year">
                <?php
                $now_year = date("Y");
                $year_start = 2000;
                while ($year_start <= $now_year) {
                    $year_val = $year_start . "-" . ($year_start + 1);
                    ?>
                    <option value="<?php echo $year_val ?>" <?php echo (!empty($doc['financial_year']) && $doc['financial_year'] == $year_val) ? 'selected="true"' : ''; ?>><?php echo $year_val; ?></option>
                    <?php
                    $year_start++;
                }
                ?>                                                
            </select>
        </p>
    </div>
    <div class="col-sm-6">
        <p class="form-control-static">
            <label class="form-label semibold" for="doc_type">Doc Type<span class="red">*</span></label>
            <select class="select2 required" name="doc_type" id="doc_type">
                <option value="">Select</option>
                <?php foreach ($doc_types as $doc_type_id => $doc_type_name){ ?>
                <option value="<?php echo $doc_type_id; ?>" <?php echo (!empty($doc['doc_type']) && $doc['doc_type'] == $doc_type_id) ? 'selected="true"' : ''; ?>><?php echo $doc_type_name; ?></option>
                <?php } ?>
            </select>
        </p>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-6">
        <p class="form-control-static">
            <label class="form-label semibold" for="doc_name">Doc Name<span class="red">*</span></label>
            <input type="text" name="doc_name" id="doc_name" class="form-control required" placeholder="Doc Name" value="<?php echo!empty($doc['doc_name']) ? $doc['doc_name'] : ""; ?>" />
        </p>
    </div>
    <div class="col-sm-6">                
        <p class="form-control-static">
            <label class="form-label semibold" for="doc_password">Doc Password</label>
            <input type="text" name="doc_password" id="doc_password" class="form-control" value="<?php echo!empty($doc['doc_password']) ? $doc['doc_password'] : ""; ?>" />
        </p>
    </div>
</div>
