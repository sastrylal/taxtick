<div id="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-typical box-typical-padding addproduct-fromgrup">
                    <h5 class="m-t-lg with-border m-t-0">Search By</h5>                    
                    <div class="form-group row ">
                        <form method="get">                            
                            <div class="col-sm-4">
                                <p class="form-control-static">
                                    <input type="text" placeholder="Doc Name" id="key" name="key" value="<?php echo!empty($_GET['key']) ? $_GET['key'] : ""; ?>" class="form-control" />
                                </p>
                            </div>                            
                            <div class="col-sm-2">
                                <p class="form-control-static">
                                    <button type="submit" class="btn btn-green">Search</button>
                                    <a href="/my/docDrive/" class="btn btn-green">Reset</a>
                                </p>
                            </div>
                        </form>
                        <div class="col-sm-2 pull-right">
                            <p class="form-control-static">
                                <a href="/my/docDrive/addDoc"><button class="btn btn-green pull-right">Add New Doc</button></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="box-typical box-typical-padding addproduct-fromgrup">
                    <?php getMessage(); ?>
                    <div class="form-group row ">
                        <div class="col-sm-12">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>S.No</th>
                                    <th>Doc Name</th>
                                    <th>Password</th>
                                    <th>Financial Year</th>
                                    <th>Doc Type</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                <?php if (!empty($docs)) { ?>
                                    <?php foreach ($docs as $doc) { ?>
                                        <tr>
                                            <td><?php echo $desc_sno--; ?></td>
                                            <td><?php echo!empty($doc['doc_name']) ? $doc['doc_name'] : ""; ?></td>
                                            <td><?php echo!empty($doc['doc_password']) ? $doc['doc_password'] : ""; ?></td>
                                            <td><?php echo!empty($doc['financial_year']) ? $doc['financial_year'] : ""; ?></td>
                                            <td><?php echo!empty($doc['doc_type_name']) ? $doc['doc_type_name'] : ""; ?></td>
                                            <td><?php echo!empty($doc['created_on']) ? dateTimeDB2SHOW($doc['created_on']) : ""; ?></td>
                                            <td>
                                                <span class="update"><a href="/my/docDrive/edit/<?php echo $doc['doc_id']; ?>/" data-remote="false" data-toggle="modal" data-target="#editmodel"><i class="fa fa-pencil-square-o update" aria-hidden="true"></i></a> </span>
                                                &nbsp; &nbsp;
                                                <span class="delete"><a href="/my/docDrive/?act=del&doc_id=<?php echo $doc['doc_id']; ?>" onclick="return confirm('Do You Want To Delete this Item?');"><i class="fa fa-times" aria-hidden="true"></i></a></span>
                                              
                                                <span class="download"><a href="/my/docDrive/download/<?php echo $doc['doc_id']; ?>/"><i class="fa fa-download" aria-hidden="true"></i></a></span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="7">No document(s) found.</td>
                                    </tr>
                                <?php } ?>                                
                            </table>
                            <?php echo!empty($PAGING) ? $PAGING : ""; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

<div id="editmodel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form name="frm" id="frm" method="post" action="" role="form" enctype="multipart/form-data">
            <input type="hidden" name="DOC_UPDATE" value="true" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="doc_title">Doc Edit</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $("#editmodel").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        //$("#doc_title").html(link.html());
        $(this).find(".modal-body").load(link.attr("href"));
    });
    /*$('#editmodel').on('hidden.bs.modal', function () {
        location.reload();
    });*/
</script>