<div id="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-typical box-typical-padding addproduct-fromgrup">
                    <h5 class="m-t-lg with-border m-t-0">Add New Doc</h5>
                    <div class="form-group row ">
                        <form name="frm" id="frm" method="post" action="" enctype="multipart/form-data">
                            <div class="col-sm-12">
                                <div class="form-group row ">
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="financial_year">Financial Year<span class="red">*</span></label>
                                            <select class="select2" name="financial_year" id="financial_year">
                                                <?php
                                                $now_year = date("Y");
                                                $year_start = 2000;
                                                while ($year_start <= $now_year) {
                                                    ?>
                                                    <option value="<?php echo $year_start . "-" . ($year_start + 1); ?>"><?php echo $year_start . "-" . ($year_start + 1); ?></option>
                                                    <?php
                                                    $year_start++;
                                                }
                                                ?>                                                
                                            </select>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="doc_type">Doc Type<span class="red">*</span></label>
                                            <select class="select2 required" name="doc_type" id="doc_type">
                                                <option value="">Select</option>
                                                <?php foreach ($doc_types as $doc_type_id => $doc_type_name) { ?>
                                                    <option value="<?php echo $doc_type_id; ?>" <?php echo (!empty($doc['doc_type']) && $doc['doc_type'] == $doc_type_id) ? 'selected="true"' : ''; ?>><?php echo $doc_type_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="doc_name">Doc Name<span class="red">*</span></label>
                                            <input type="text" name="doc_name" id="doc_name" class="form-control required" placeholder="Doc Name" />
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="doc_path">Upload Doc<span class="red">*</span></label>
                                            <input type="file" name="doc_path" id="doc_path" class="required" />
                                        </p>

                                    </div>
                                </div>  

                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <p class="form-control-static" style="display: none;" id="doc_pass_div">
                                            <label class="form-label semibold" for="doc_password">Doc Password</label>
                                            <input type="text" name="doc_password" id="doc_password" class="form-control" />
                                        </p>
                                    </div>
                                </div>                                
                                <div class="form-group row mgb0">
                                    <div class="col-sm-12">
                                        </br>
                                        <h5 class="with-border"></h5>
                                        <span class="pull-right">
                                            <button type="submit" class="btn btn-green">Submit</button> 
                                            <a href="/my/DocDrive/" class="btn btn-secondary">Cancel</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#doc_path").on("change", function () {
            var doc_path = $("#doc_path").val();
            if (doc_path.length > 0) {
                $("#doc_pass_div").show();
            } else {
                $("#doc_pass_div").hide();
            }
        });
        $("#frm").validate({});
        $("#doc_pass_div").hide();
    });
</script>