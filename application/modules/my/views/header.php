<!Doctype html>
<html>
    <head>        
        <meta charset="utf-8">           
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        
        <title>Taxtick - Dashboard</title>
        <link rel="apple-touch-icon" sizes="57x57" href="/images/fav/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/images/fav/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/images/fav/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/images/fav/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/images/fav/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/images/fav/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/images/fav/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/images/fav/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/images/fav/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/images/fav/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/images/fav/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/images/fav/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/images/fav/favicon-16x16.png">
        <link rel="manifest" href="/images/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/images/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        
        
        <link rel="stylesheet" href="/css/bootstrap.min.css" />       
        <link rel="stylesheet" href="/css/dashboard.css" />                
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />        
        <script src="/js/jquery.min.js"></script>        
        <script src="/js/custom.js"></script>
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
        <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
    </head>
    <body>
        <header id="site-header">            
            <div class="container-fluid">                
                <div class="row">                    
                    <div class="col-md-3"><a href="/my/"><img src="/images/dashboard-logo.png"/></a></div>                    
                    <div class="col-md-9 pull-right">
                        <span class="pull-right top-menu"><a href="/my/profile/">Profile</a> | <a href="/my/logout/">Logout</a></span>
                    </div>                
                </div>            
            </div>       
        </header>       
        <nav id="side-menu">            
            <ul>
                <!--li class="grey">                    
                    <a href="/my/">                       
                        <i class="fa fa-arrows-alt" aria-hidden="true"></i>                       
                        <span class="lbl">Dashboard</span> 
                    </a>
                </li-->                             
                <li class="grey">                 
                    <a href="/my/slotBooking/">  
                        <i class="fa fa-television" aria-hidden="true"></i>  
                        <span class="lbl">Block Your Slot</span>              
                    </a>           
                </li>
                <li class="grey">       
                    <a href="/my/history/" >          
                        <i class="fa fa-retweet" aria-hidden="true"></i> 
                        <span class="lbl">History</span>             
                    </a>
                </li> 
                <li class="grey">   
                    <a href="/my/docDrive/" >      
                        <i class="fa fa-folder" aria-hidden="true"></i>     
                        <span class="lbl">Doc Drive</span>             
                    </a>             
                </li>          
                <li class="grey">         
                    <a href="/my/faqs/" >   
                        <i class="fa fa-question-circle" aria-hidden="true"></i>        
                        <span class="lbl">FAQ's</span>                
                    </a>       
                </li>              
                <li class="grey">                 
                    <a href="/my/askus/" >                 
                        <i class="fa fa-twitch" aria-hidden="true"></i>   
                        <span class="lbl">Ask Us</span>                
                    </a>            
                </li>            
            </ul>   
        </nav>
