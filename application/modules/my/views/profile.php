<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<div id="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <?php getMessage(); ?>
                <div class="box-typical box-typical-padding addproduct-fromgrup">
                    <h5 class="m-t-lg with-border m-t-0">Profile</h5>
                    <div class="form-group row ">
                        <div class="col-sm-12">
                            <form id="frm" name="frm" method="post" action="">
                                <input type="hidden" name="PROFILE" value="true" />
                                <div class="form-group row ">
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="customer_name">First Name *</label>
                                            <input type="text" placeholder="First Name" id="first_name" name="first_name" class="form-control required" value="<?php echo!empty($customer['first_name']) ? $customer['first_name'] : ""; ?>" />
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="email">Last Name *</label>
                                            <input type="text" placeholder="Last Name" id="last_name" name="last_name" class="form-control required" value="<?php echo!empty($customer['last_name']) ? $customer['last_name'] : ""; ?>"  />
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="mobile">Mobile Number *</label>
                                            <input type="text" placeholder="Mobile" id="mobile" name="mobile" class="form-control required" value="<?php echo!empty($customer['mobile']) ? $customer['mobile'] : ""; ?>" />
                                        </p>
                                    </div>
                                </div>  
                                <div class="form-group row mgb0">
                                    <div class="col-sm-12">
                                        </br>
                                        <h5 class="with-border"></h5>
                                        <span class="pull-right">
                                            <button type="submit" class="btn btn-green">Update</button> 
                                            <a href="/my/profile/" class="btn btn-secondary">Cancel</a>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            </br>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <div class="col-sm-12">
                            <h5 class="m-t-lg with-border m-t-0">Change Password</h5>
                            <form id="frm_pwd" name="frm_pwd" method="post" action="">
                                <input type="hidden" name="PWD" value="true" />
                                <div class="form-group row ">                                    
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="email">New Password *</label>
                                            <input type="password" name="password" id="password" class="form-control required" value=""  />
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <label class="form-label semibold" for="mobile">Confirm Password *</label>
                                            <input type="password" name="confirm_password" id="confirm_password" class="form-control required" value="" />
                                        </p>
                                    </div>
                                </div>                                                   
                                <div class="form-group row mgb0">
                                    <div class="col-sm-12">
                                        </br>
                                        <h5 class="with-border"></h5>
                                        <span class="pull-right">
                                            <button type="submit" class="btn btn-green">Update</button> 
                                            <a href="/my/profile/" class="btn btn-secondary">Cancel</a>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#frm").validate();
        $("#frm_pwd").validate({
            rules: {
                password: {
                    minlength: 5,
                    required: true
                },
                confirm_password: {
                    minlength: 5,
                    required: true,
                    equalTo: "#password"
                }
            }
        });
    });
</script>