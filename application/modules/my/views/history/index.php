<div id="page-content">   
    <div class="container-fluid">     
        <div class="row">        
            <div class="col-lg-12">            
                <div class="box-typical box-typical-padding addproduct-fromgrup">    
                    <h5 class="m-t-lg with-border m-t-0">History</h5>           
                    <?php getMessage(); ?>         
                    <div class="form-group row ">              
                        <div class="col-sm-12">
                            <table class="table table-striped table-bordered">                 
                                <tr>
                                    <th>#</th>
                                    <th>Slot Date</th>
                                    <th>Purpose of Scheduling</th>
                                    <th>Sharing Software</th>
                                    <th>Amount</th>            
                                    <th>Status</th>
                                    <th width="80px;">Action</th>
                                </tr>
                                <?php if (!empty($slots)) { ?>
                                    <?php foreach ($slots as $slot) { ?>
                                        <tr>
                                            <td><?php echo $desc_sno--; ?></td>
                                            <td>
                                                <?php echo!empty($slot['slot_date']) ? dateTimeDB2SHOW($slot['slot_date']) : ""; ?>
                                                <?php if (!empty($slot['reschedule'])) { ?>
                                                    <a href="#" data-remote="false" data-toggle="modal" data-target="#reschedule_content">(R)</a>
                                                    <div style="display: none;" id="reschedule_content<?php echo $slot['slot_id']; ?>"><?php echo $slot['reschedule']; ?></div>
                                                <?php } ?>
                                            </td>     
                                            <td><?php echo!empty($slot['purpose']) ? $slot['purpose'] : ""; ?></td>       
                                            <td><?php echo!empty($slot['sharing_software']) ? $slot['sharing_software'] : ""; ?></td>       
                                            <td><?php echo!empty($slot['amount']) ? $slot['amount'] : ""; ?></td>
                                            <td <?php echo ($slot['slot_status'] == "Completed") ? 'style="background-color: #00ff00;"' : 'style="background-color: yellow;"'; ?>><?php echo!empty($slot['slot_status']) ? $slot['slot_status'] : ""; ?></td>
                                            <td class="retwet">
                                                <?php if ($slot['slot_status'] != "Completed") { ?>
                                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#reScheduleModel"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="7">No Slot(s) found.</td>
                                    </tr>
                                <?php } ?>
                            </table>
                            <?php echo!empty($PAGING) ? $PAGING : ""; ?>
                        </div>
                    </div>               
                </div>   
            </div>   
        </div> 
    </div>
</div>
<div id="reScheduleModel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reschedule</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    Please contact our helpdesk @ +91 903-008-3030 for re-schedule your Slot
                </div>                    
            </div>
        </div>
    </div>
</div>
<div id="reschedule_content" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Slot Reschedule</h5>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#reschedule_content").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").html(link.next().html());
    });
</script>