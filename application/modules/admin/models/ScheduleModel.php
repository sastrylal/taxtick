<?php

class ScheduleModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getScheduleDays($user_id) {
        $this->db->select("*");
        $this->db->where("user_id", $user_id);
        $this->db->join("tbl_week_days s", "m.schedule_day=s.day_id");
        $this->db->order_by("m.schedule_day ASC");
        $query = $this->db->get("tbl_schedule_settings m");
        if ($query->num_rows() > 0) {            
            return $query->result_array();
        }
        return false;
    }
    
    function getDefaultScheduleDays() {
        $this->db->select("*");        
        $this->db->order_by("day_id ASC");
        $query = $this->db->get("tbl_week_days");
        if ($query->num_rows() > 0) {            
            return $query->result_array();
        }
        return false;
    }
    
    function saveScheduleDays($user_id, $day){
        $this->db->select("m.*");
        $this->db->where("m.user_id", $user_id);
        $this->db->where("m.schedule_day", $day['schedule_day']);
        $query = $this->db->get("tbl_schedule_settings m");
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $day['user_id'] = $user_id;
            $this->db->where("schedule_id", $row['schedule_id']);
            return $this->db->update("tbl_schedule_settings", $day);
        }else{
            $day['user_id'] = $user_id;
            $this->db->insert("tbl_schedule_settings", $day);
            return $this->db->insert_id();
        }
        return false;
    }
            
    function addScheduleDay($pdata) {
        $this->db->insert("tbl_schedule_settings", $pdata);
        return $this->db->insert_id();
    }

    function updateScheduleDay($schedule_id, $pdata) {
        $this->db->where("schedule_id", $schedule_id);
        return $this->db->update("tbl_schedule_settings", $pdata);
    }

    function delScheduleDay($schedule_id) {
        $this->db->where("schedule_id", $schedule_id);
        return $this->db->delete("tbl_schedule_settings");
    }
    
}

?>