<?php

class AdminModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    //Askus
    
    
    //Customers
    function addCustomer($pdata) {
        $this->db->insert("tbl_customers", $pdata);
        return $this->db->insert_id();
    }

    function updateCustomer($pdata, $customer_id) {
        $this->db->where("customer_id", $customer_id);
        return $this->db->update("tbl_customers", $pdata);
    }

    function delCustomer($customer_id) {
        $this->db->where("customer_id", $customer_id);
        return $this->db->delete("tbl_customers");
    }

    function getCustomerById($customer_id) {
        $this->db->select("m.*");
        $this->db->where("customer_id", $customer_id);
        $query = $this->db->get("tbl_customers m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchCustomers($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*");
            //$this->db->select("(SELECT GROUP_CONCAT(DATE_FORMAT(s.slot_date, '%d/%m/%Y %h:%i %p') SEPARATOR '<br/>') FROM tbl_slots s WHERE s.customer_id=m.customer_id AND s.payment_status = 'Paid' AND s.slot_status = 'Completed' LIMIT 10) AS completed_slots");
            $this->db->select("(SELECT COUNT(1) FROM tbl_slots s WHERE s.customer_id=m.customer_id AND s.payment_status = 'Paid' AND s.slot_status = 'Completed') AS completed_slots");
            $this->db->select("(SELECT GROUP_CONCAT(DATE_FORMAT(s.slot_date, '%d/%m/%Y %h:%i %p') SEPARATOR '<br/>') FROM tbl_slots s WHERE s.customer_id=m.customer_id AND s.payment_status = 'Paid' AND s.slot_status = 'In Process' LIMIT 10) AS in_process_slots");
        }
        if (!empty($s['key'])) {
            $this->db->where("(m.first_name LIKE '%" . $s['key'] . "%' || m.last_name LIKE '%" . $s['key'] . "%' || m.email LIKE '%" . $s['key'] . "%' || m.mobile LIKE '%" . $s['key'] . "%')");
        }
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->order_by("m.customer_id DESC");
        $query = $this->db->get("tbl_customers m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function searchCustomerDocs($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*");
        }
        $this->db->where("m.customer_id", $s['customer_id']);
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->order_by("m.doc_id DESC");
        $query = $this->db->get("tbl_drive m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }
    
    function getDocById($doc_id){
        $this->db->select("m.*");
        $this->db->where("doc_id", $doc_id);
        $query = $this->db->get("tbl_drive m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    //Users
    function userLogin($username, $password) {
        $this->db->select("*");
        $this->db->where("username", $username);
        $this->db->where("password", $password);
        $this->db->where("is_active", "1");
        $query = $this->db->get("tbl_users");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    // Manage Users nothing but Admin
    function addUser($pdata) {
        $this->db->insert("tbl_users", $pdata);
        return $this->db->insert_id();
    }

    function updateUser($user_id, $pdata) {
        $this->db->where("user_id", $user_id);
        return $this->db->update("tbl_users", $pdata);
    }

    function delUser($user_id) {
        $this->db->where("user_id", $user_id);
        return $this->db->delete("tbl_users");
    }

    function getUserById($user_id) {
        $this->db->select("m.*");
        $this->db->where("user_id", $user_id);
        $query = $this->db->get("tbl_users m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
    function getUserByEmail($email) {
        $this->db->select("m.*");
        $this->db->where("m.email", $email);
        $query = $this->db->get("tbl_users m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
    

    function searchUsers($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, r.role_name");
        }
        if (!empty($s['key'])) {
            $this->db->where("(m.first_name LIKE '%" . $s['key'] . "%' || m.last_name LIKE '%" . $s['key'] . "%' || m.username LIKE '%" . $s['key'] . "%' || m.role LIKE '%" . $s['key'] . "%')");
        }
        $this->db->join("tbl_roles r", "m.role_id=r.role_id", "left");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->order_by("m.user_id DESC");
        $query = $this->db->get("tbl_users m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

}

?>