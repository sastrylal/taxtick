<?php

class SlotModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getUsersList() {
        $this->db->select("user_id, first_name, last_name");
        $this->db->where("is_active", "1");
        $query = $this->db->get("tbl_users");
        if ($query->num_rows() > 0) {
            $users = [];
            foreach ($query->result_array() as $row) {
                $users[$row['user_id']] = $row['first_name'];
            }
            return $users;
        }
        return false;
    }

    function searchSlots($s, $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, u.first_name AS user_name");
            $this->db->join("tbl_users u", "m.assign_to=u.user_id", "left");
        }
        if (!empty($s['key'])) {
            $this->db->where("m.customer_name LIKE '%".$s['key']."%' OR m.email LIKE '%".$s['key']."%' OR m.mobile LIKE '%".$s['key']."%'");
        }
        if (!empty($s['slot_date'])) {
            $this->db->where("DATE(m.slot_date)", $s['slot_date']);
        }
        if (!empty($s['assign_to'])) {
            $this->db->where("m.assign_to", $s['assign_to']);
        }
        if (!empty($s['status'])) {
            $this->db->where("m.slot_status", $s['status']);
        }
        $this->db->where("m.payment_status", "Paid");
        $this->db->order_by("m.slot_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("tbl_slots m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function addSlot($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_slots", $pdata);
        return $this->db->insert_id();
    }

    function updateSlot($slot_id, $pdata) {
        $this->db->where("slot_id", $slot_id);
        return $this->db->update("tbl_slots", $pdata);
    }

    function delSlot($slot_id) {
        $this->db->where("slot_id", $slot_id);
        return $this->db->delete("tbl_slots");
    }

    function getSlotById($slot_id) {
        $this->db->select("m.*, u.first_name AS user_name");
        $this->db->join("tbl_users u", "m.assign_to=u.user_id", "left");
        $this->db->where("m.slot_id", $slot_id);
        $query = $this->db->get("tbl_slots m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function getAvailableSlotTimes() {
        $available_slot_times = [];
        $i = 0;
        $nowDate = new DateTime(date("Y-m-d") . " 10:00:00");
        $endDate = new DateTime(date("Y-m-d") . " 18:00:00");
        while ($nowDate <= $endDate) {
            $slotKey = $nowDate->format("H:i:s");
            $slotVal = $nowDate->format("h:i A");
            $nowDate->modify("+1 20min");
            $slotVal = $slotVal . " - " . $nowDate->format("h:i A");
            $available_slot_times[$slotKey] = $slotVal;
            $nowDate->modify("+1 10min");
        }
        return $available_slot_times;
    }

}

?>