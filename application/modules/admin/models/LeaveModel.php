<?php
class LeaveModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    function searchLeaves($s, $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*", false);
        }
        if(isset($s['user_id'])){
            $this->db->where("m.user_id", $s['user_id']);
        }
        $this->db->order_by("m.leave_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("tbl_leaves m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }
    function addLeave($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_leaves", $pdata);
        return $this->db->insert_id();
    }
    function updateLeave($leave_id, $pdata) {
        $this->db->where("leave_id", $leave_id);
        return $this->db->update("tbl_leaves", $pdata);
    }
    function delLeave($leave_id) {
        $this->db->where("leave_id", $leave_id);
        return $this->db->delete("tbl_leaves");
    }
    function getLeaveById($leave_id) {
        $this->db->select("m.*");
        $this->db->where("m.leave_id", $leave_id);
        $query = $this->db->get("tbl_leaves m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
}
?>