<?php
class FaqModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    function searchQuestions($s, $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.faq_id, m.title, SUBSTRING(m.description, 1, 200) AS description, m.is_active, m.created_on", false);
        }
        if(isset($s['is_active'])){
            $this->db->where("m.is_active", $s['is_active']);
        }
        $this->db->order_by("m.faq_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("tbl_faqs m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }
    function addQuestion($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_faqs", $pdata);
        return $this->db->insert_id();
    }
    function updateQuestion($faq_id, $pdata) {
        $this->db->where("faq_id", $faq_id);
        return $this->db->update("tbl_faqs", $pdata);
    }
    function delQuestion($faq_id) {
        $this->db->where("faq_id", $faq_id);
        return $this->db->delete("tbl_faqs");
    }
    function getQuestionById($faq_id) {
        $this->db->select("m.*");
        $this->db->where("m.faq_id", $faq_id);
        $query = $this->db->get("tbl_faqs m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
}
?>