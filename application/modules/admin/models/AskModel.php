<?php

class AskModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function searchAsks($s, $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, CONCAT(c.first_name, ' ', c.last_name) AS customer_name, u.first_name AS user_name");
            $this->db->join("tbl_users u", "m.assign_to=u.user_id", "left");
        }
        $this->db->join("tbl_customers c", "m.customer_id=c.customer_id", "left");
        if (!empty($s['key'])) {
            $this->db->where("(m.title LIKE '%" . $s['key'] . "%' OR c.first_name LIKE '%" . $s['key'] . "%' OR c.last_name LIKE '%" . $s['key'] . "%' )");
        }
         if (!empty($s['assign_to'])) {
            $this->db->where("m.assign_to", $s['assign_to']);
        }
        $this->db->order_by("m.ask_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("tbl_askus m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function addAsk($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_askus", $pdata);
        return $this->db->insert_id();
    }

    function updateAsk($ask_id, $pdata) {
        $this->db->where("ask_id", $ask_id);
        return $this->db->update("tbl_askus", $pdata);
    }

    function delAsk($ask_id) {
        $this->db->where("ask_id", $ask_id);
        return $this->db->delete("tbl_askus");
    }

    function getAskById($ask_id) {
        $this->db->select("m.*, c.first_name AS customer_name, c.email, c.mobile");
        $this->db->join("tbl_customers c", "m.customer_id=c.customer_id");
        $this->db->where("m.ask_id", $ask_id);
        $query = $this->db->get("tbl_askus m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function addAskMessage($pdata) {
        $this->db->set("created_on", date("Y-m-d H:i:s"));
        $this->db->insert("tbl_ask_messages", $pdata);
        return $this->db->insert_id();
    }

    function updateAskMessage($msg_id, $pdata) {
        $this->db->where("msg_id", $msg_id);
        return $this->db->update("tbl_ask_messages", $pdata);
    }

    function delAskMessage($msg_id) {
        $this->db->where("msg_id", $msg_id);
        return $this->db->delete("tbl_ask_messages");
    }

    function getAskMessages($ask_id) {
        $this->db->where("ask_id", $ask_id);
        $this->db->order_by("m.msg_id DESC");
        $query = $this->db->get("tbl_ask_messages m");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

}

?>