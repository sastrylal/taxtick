<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    
class Promos extends MY_Controller {
    
    function __construct(){
        parent::__construct();        
        $this->load->model("PromoModel", "promoModel", true);
        $this->_admin_check();
    }
    
    public function index()
    {
        $data = array();
        if($this->input->get("act") !== null && $this->input->get("act") == "del"){
            $this->promoModel->delPromo($this->input->get("promo_id"));
            setMessage("Your promo has been deleted successfully!");
            redirect(base_url()."admin/promos/");
        }
        if($this->input->get("act") !== null && $this->input->get("act") == "status"){
            $is_active = $this->input->get("sta") == "1"?"1":"0";
            $this->promoModel->updatePromo($this->input->get("promo_id"), ["is_active" => $is_active]);
            setMessage("Your promo status has been updated successfully!");
            redirect(base_url()."admin/promos/");
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = array();
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/promos/?';
        $this->pagenavi->process($this->promoModel, 'searchPromos');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['promos'] = $this->pagenavi->items;
        $this->_template("promos/index", $data);
    }
    
    public function add(){
        $data = array();
        if($this->input->post("promo_code") !== null){
            $pdata = array();
            $pdata['promo_code'] = $this->input->post("promo_code");
            $pdata['promo_type'] = $this->input->post("promo_type");
            $pdata['discount'] = $this->input->post("discount");
            $promo_id = $this->promoModel->addPromo($pdata);            
            setMessage("Your promo has been added successfully!");
            redirect(base_url()."admin/promos/");
        }        
        $this->_template("promos/form", $data);
    }
    
    public function edit(){
        $data = array();
        if($this->input->post("promo_id") !== null){
            $pdata = array();
            $pdata['promo_code'] = $this->input->post("promo_code");
            $pdata['promo_type'] = $this->input->post("promo_type");
            $pdata['discount'] = $this->input->post("discount");
            $this->promoModel->updatePromo($this->input->post("promo_id"), $pdata);            
            setMessage("Your promo has been updated successfully!");
            redirect(base_url()."admin/promos/");
        }
        $data['promo'] = $this->promoModel->getPromoById($this->input->get("promo_id"));        
        $this->_template("promos/form", $data);
    }
    
    public function check_promo_code(){
        if (!empty($_GET['promo_code'])) {
            if ($this->promoModel->getPromoByCode($_GET['promo_code']) === false) {
                echo "true";
                exit;
            } else {
                echo "false";
                exit;
            }
        }
        echo "false";
        exit;
    }

}

?>