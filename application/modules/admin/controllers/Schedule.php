<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("ScheduleModel", "scheduleModel", true);
        $this->_admin_check();
    }

    public function index() {
        $data = array();
        if (!empty($_POST['week_days'])) {
            foreach ($_POST['week_days'] as $day_id => $day_working) {
                $this->scheduleModel->saveScheduleDays($_SESSION['USER_ID'], [
                    "schedule_day" => $day_id,
                    "is_working" => $day_working
                ]);
            }
            redirect(base_url() . "admin/schedule/");
        }
        $schedule_days = $this->scheduleModel->getScheduleDays($_SESSION['USER_ID']);
        if (empty($schedule_days)) {
            $schedule_days = $this->scheduleModel->getDefaultScheduleDays();
        }
        $data['schedule_days'] = $schedule_days;
        $this->_template("schedule/form", $data);
    }

}
