<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "adminModel", true);        
    }

    public function index() {
        $data = [];
        if (!empty($_POST['username'])) {
            $user = $this->adminModel->userLogin($_POST['username'], $_POST['password']);
            $_SESSION = array();
            if (!empty($user['user_id'])) {
                $_SESSION['NAME'] = $user['first_name'];
                $_SESSION['USER_ID'] = $user['user_id'];
                $_SESSION['ROLE'] = $user['role_id'];
                redirect(base_url() . "admin/");
            }
        }
        $this->load->view("login", $data);
    }

}
