<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Askus extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("AskModel", "askModel", true);
        $this->_admin_check();
    }

    public function index() {
        $data = array();
        if ($this->input->get("act") !== null && $this->input->get("act") == "del") {
            $this->askModel->delAsk($this->input->get("ask_id"));
            setMessage("Your question has been deleted successfully!");
            redirect(base_url() . "admin/askus/");
        }
        $search_data = [];
        if (!empty($_GET['key'])) {
            $search_data['key'] = $_GET['key'];
        }
        if ($_SESSION['ROLE'] == "ADMIN" && !empty($_GET['assign'])) {
            $search_data['assign_to'] = $_GET['assign'];
        } else if ($_SESSION['ROLE'] != "ADMIN") {
            $search_data['assign_to'] = $_SESSION['USER_ID'];
        }
        $this->load->model("SlotModel", "slotModel", true);
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/askus/?';
        $this->pagenavi->process($this->askModel, 'searchAsks');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['asks'] = $this->pagenavi->items;
        $data['users'] = $this->slotModel->getUsersList();
        $this->_template("askus/index", $data);
    }

    public function ask_messages($ask_id) {
        $data = [];
        if (!empty($_POST['ASKMSG']) && !empty($_POST['message'])) {
            $msg_id = $this->askModel->addAskMessage([
                "ask_id" => (!empty($_POST['ask_id']) ? $_POST['ask_id'] : ""),
                "message_by" => "Taxtick",
                "user_id" => $_SESSION['USER_ID'],
                "message" => (!empty($_POST['message']) ? $_POST['message'] : "")
            ]);
            $attach = docUpload("attach", "/data/ask/", "doc" . $msg_id);
            if (!empty($attach)) {
                $this->askModel->updateAskMessage($msg_id, [
                    "attach_name" => $_FILES["attach"]["name"],
                    "attach_path" => $attach
                ]);
            }
            $this->askModel->updateAsk($_POST['ask_id'], [
                "status" => "Replied"
            ]);

            $ask = $this->askModel->getAskById($_POST['ask_id']);
            $data['to'] = $ask['email'];
            $data['subject'] = "Congratulations!! Your Tax Request Served Successfully";
            $this->sendEmail("email/askus_reply", $data);
            $this->sendSMS($ask['mobile'], "Your query has been addressed by our experts. To view the reply, login to your account – www.taxtick.in");
            setMessage("Your message has been sent.");
            redirect(base_url() . "admin/askus/ask_messages/" . $ask_id . "/");
        }
        if (!empty($ask_id)) {
            $data['ask'] = $this->askModel->getAskById($ask_id);
            $data['messages'] = $this->askModel->getAskMessages($ask_id);
            $this->_iframe("askus/ask_messages", $data);
        }
    }

    public function edit() {
        $data = array();
        if ($this->input->post("ask_id") !== null) {
            $pdata = array();
            $pdata['title'] = $this->input->post("title");
            $pdata['description'] = $this->input->post("description");
            $this->askModel->updateAsk($this->input->post("ask_id"), $pdata);
            setMessage("Your question has been updated successfully!");
            redirect(base_url() . "admin/askus/");
        }
        $data['question'] = $this->askModel->getAskById($this->input->get("ask_id"));
        $this->_template("askus/form", $data);
    }

    public function change_assign_to() {
        if (!empty($_POST['ask_id']) && !empty($_POST['assign_to'])) {
            $this->askModel->updateAsk($_POST['ask_id'], [
                "assign_to" => $_POST['assign_to']
            ]);
        }
    }

}
