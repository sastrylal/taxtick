<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends MY_Controller {

    function __construct(){
        parent::__construct();        
        $this->load->model("FaqModel", "faqModel", true);
        $this->_admin_check();
    }
    
    public function index()
    {
        $data = array();
        if($this->input->get("act") !== null && $this->input->get("act") == "del"){
            $this->faqModel->delQuestion($this->input->get("faq_id"));
            setMessage("Your question has been deleted successfully!");
            redirect(base_url()."admin/faqs/");
        }
        if($this->input->get("act") !== null && $this->input->get("act") == "status"){
            $is_active = $this->input->get("sta") == "1"?"1":"0";
            $this->faqModel->updateQuestion($this->input->get("faq_id"), ["is_active" => $is_active]);
            setMessage("Your question status has been updated successfully!");
            redirect(base_url()."admin/faqs/");
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = array();
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/faqs/?';
        $this->pagenavi->process($this->faqModel, 'searchQuestions');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['questions'] = $this->pagenavi->items;
        $this->_template("faqs/index", $data);
    }

    public function add(){
        $data = array();
        if($this->input->post("title") !== null){
            $pdata = array();
            $pdata['title'] = $this->input->post("title");
            $pdata['description'] = $this->input->post("description");
            $faq_id = $this->faqModel->addQuestion($pdata);            
            setMessage("Your question has been added successfully!");
            redirect(base_url()."admin/faqs/");
        }        
        $this->_template("faqs/form", $data);
    }

    public function edit(){
        $data = array();
        if($this->input->post("faq_id") !== null){
            $pdata = array();
            $pdata['title'] = $this->input->post("title");
            $pdata['description'] = $this->input->post("description");
            $this->faqModel->updateQuestion($this->input->post("faq_id"), $pdata);            
            setMessage("Your question has been updated successfully!");
            redirect(base_url()."admin/faqs/");
        }
        $data['question'] = $this->faqModel->getQuestionById($this->input->get("faq_id"));        
        $this->_template("faqs/form", $data);
    }

}