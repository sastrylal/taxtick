<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "adminModel", TRUE);
        $this->_admin_check();
    }

    public function index() {
        redirect(base_url() . "admin/slots/");
        $data = [];
        $this->_template("index", $data);
    }

    public function profile() {
        $data = [];
        if (!empty($_POST['PROFILE'])) {
            $this->adminModel->updateUser($_SESSION['USER_ID'], [
                "first_name" => !empty($_POST['first_name']) ? $_POST['first_name'] : "",
                "last_name" => !empty($_POST['last_name']) ? $_POST['last_name'] : ""
            ]);
            setMessage("Your profile has been updated");
            redirect(base_url()."admin/profile/");
        } else if (!empty($_POST['PWD'])) {
            $this->adminModel->updateUser($_SESSION['USER_ID'], [
                "password" => !empty($_POST['password']) ? $_POST['password'] : ""
            ]);
            setMessage("Your password has been updated");
            redirect(base_url()."admin/profile/");
        }
        $data['user'] = $this->adminModel->getUserById($_SESSION['USER_ID']);
        $this->_template("profile", $data);
    }

    public function logout() {
        $_SESSION = [];
        redirect(base_url() . "admin/login/");
    }

}

?>