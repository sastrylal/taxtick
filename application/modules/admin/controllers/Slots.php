<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slots extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("SlotModel", "slotModel", true);
        $this->load->model("DBModel", "dbModel", true);
        $this->_admin_check();
    }

    public function index() {
        $data = array();
        if ($this->input->get("act") !== null && $this->input->get("act") == "del") {
            $this->slotModel->delSlot($this->input->get("slot_id"));
            setMessage("Your slot has been deleted successfully!");
            redirect(base_url() . "admin/slots/");
        }
        if ($this->input->get("act") !== null && $this->input->get("act") == "status") {
            $is_active = $this->input->get("sta") == "1" ? "1" : "0";
            $this->slotModel->updateSlot($this->input->get("slot_id"), ["is_active" => $is_active]);
            setMessage("Your slot status has been updated successfully!");
            redirect(base_url() . "admin/slots/");
        }
        $search_data = [];
        if (!empty($_GET['status'])) {
            $search_data['status'] = $_GET['status'];
        }
        if (!empty($_GET['key'])) {
            $search_data['key'] = $_GET['key'];
        }
        if (!empty($_GET['slot_date']) && $_GET['slot_date'] == "All") {
            $_GET['slot_date'] = "";
        } else if (!empty($_GET['slot_date'])) {
            $search_data['slot_date'] = dateForm2DB($_GET['slot_date']);
        } else if (empty($search_data) && !isset($_GET['key'])) {
            $search_data['slot_date'] = date("Y-m-d");
            $_GET['slot_date'] = date("d/m/Y");
        }
        if ($_SESSION['ROLE'] == "ADMIN" && !empty($_GET['assign'])) {
            $search_data['assign_to'] = $_GET['assign'];
        } else if ($_SESSION['ROLE'] != "ADMIN") {
            $search_data['assign_to'] = $_SESSION['USER_ID'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/slots/?';
        $this->pagenavi->process($this->slotModel, 'searchSlots');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['slots'] = $this->pagenavi->items;
        $data['users'] = $this->slotModel->getUsersList();
        $this->_template("slots/index", $data);
    }

    public function assign($slot_id) {
        $data = [];
        if ($this->input->post("slot_id") !== null && $this->input->post("assign_to") !== null) {
            $this->slotModel->updateSlot($this->input->post("slot_id"), [
                "assign_to" => $this->input->post("assign_to")
            ]);
            setMessage("Slot has been updated successfully.");
            redirect(base_url() . "admin/slots/assign/" . $this->input->post("slot_id") . "/");
        }
        $data['slot'] = $this->slotModel->getSlotById($slot_id);
        $data['users'] = $this->slotModel->getUsersList();
        $this->_iframe("slots/assign", $data);
    }

    public function schedule($slot_id) {
        $data = [];
        if ($this->input->post("slot_id") !== null && $this->input->post("slot_date") !== null && $this->input->post("slot_time") !== null) {
            $slot = $this->slotModel->getSlotById($this->input->post("slot_id"));
            $reschedule = $slot['reschedule'].$_POST['slot_date']." ".$_POST['slot_time']."<br/>";
            $this->slotModel->updateSlot($this->input->post("slot_id"), [
                "slot_date" => dateForm2DB($_POST['slot_date']) . " " . $_POST['slot_time'],
                "reschedule" => $reschedule
            ]);
            $slot = $this->slotModel->getSlotById($this->input->post("slot_id"));
            $slot_date_time = new DateTime($slot['slot_date']);
            $slot['slot_date_time'] = $slot_date_time;
            $slot['to'] = $slot['email'];
            $slot['subject'] = "Reschedule Confirmation - " . $slot_date_time->format("d-m-Y") . " at " . $slot_date_time->format("G:iA") . " (www.taxtick.in)";
            $this->sendEmail("email/slot_reschedule", $slot);
            $this->sendSMS($slot['mobile'], "We confirm that we have successfully processed your reschedule request. Your new slot is (" . $slot_date_time->format("d-m-Y") . " at " . $slot_date_time->format("G:iA") . ") – www.taxtick.in");
            setMessage("Slot has been updated successfully.");
            redirect(base_url() . "admin/slots/schedule/" . $this->input->post("slot_id") . "/");
        }
        $data['slot'] = $this->slotModel->getSlotById($slot_id);
        $data['users'] = $this->slotModel->getUsersList();
        $this->_iframe("slots/schedule", $data);
    }

    public function available_slot_times() {
        $slot_times = [];
        if (!empty($_POST['slot_date'])) {
            $slot_times = $this->slotModel->getAvailableSlotTimes($_POST['slot_date']);
        }
        header('Content-Type: application/json');
        echo json_encode($slot_times);
    }

    public function change_status() {
        if (!empty($_POST['slot_id']) && !empty($_POST['slot_status'])) {
            $this->slotModel->updateSlot($_POST['slot_id'], [
                "slot_status" => $_POST['slot_status']
            ]);
            if ($_POST['slot_status'] == "Completed") {
                $slot = $this->slotModel->getSlotById($this->input->post("slot_id"));                
                $slot['to'] = $slot['email'];
                $slot['subject'] = "Congratulations!! Your Tax Request Served Successfully";
                $this->sendEmail("email/slot_completed", $slot);
                $this->sendSMS($slot['mobile'], "Congratulations!! We successfully served your request. If you like our service, refer to your near and dear – www.taxtick.in");
            }
        }
    }

    public function change_assign_to() {
        if (!empty($_POST['slot_id']) && !empty($_POST['assign_to'])) {
            $this->slotModel->updateSlot($_POST['slot_id'], [
                "assign_to" => $_POST['assign_to']
            ]);
        }
    }

    public function view($slot_id) {
        $data = array();
        $data['slot'] = $this->slotModel->getSlotById($slot_id);
        $this->load->view("slots/view", $data);
    }
    
    public function available() {
        $data = array();        
        $search_data = [];
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/slots/available/?';
        $this->pagenavi->process($this->slotModel, 'searchAvailableSlots');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['slots'] = $this->pagenavi->items;        
        $this->_template("slots/available", $data);
    }

}
