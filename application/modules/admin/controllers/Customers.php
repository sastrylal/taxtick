<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "adminModel", true);
        $this->_admin_check();
    }

    public function index() {
        $data = array();
        if ($this->input->get("act") !== null && $this->input->get("act") == "del") {
            $this->adminModel->delCustomer($this->input->get("customer_id"));
            setMessage("Your customer has been deleted successfully!");
            redirect(base_url() . "admin/customers/");
        }
        if ($this->input->get("act") !== null && $this->input->get("act") == "status") {
            $is_active = $this->input->get("sta") == "1" ? "1" : "0";
            $this->adminModel->updateCustomer($this->input->get("customer_id"), ["is_active" => $is_active]);
            setMessage("Your customer status has been updated successfully!");
            redirect(base_url() . "admin/customers/");
        }
        $search_data = [];        
        if (!empty($_GET['key'])) {
            $search_data['key'] = $_GET['key'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/customers/?';
        $this->pagenavi->process($this->adminModel, 'searchCustomers');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['customers'] = $this->pagenavi->items;
        $this->_template("customers/index", $data);
    }

    public function view() {
        $data = [];
        $data['customer'] = $this->adminModel->getCustomerById($this->input->get("customer_id"));
        $this->load->view("customers/view", $data);
    }

    public function docs($customer_id) {
        if (!empty($customer_id)) {
            $data = [];
            $search_data = [];
            $search_data['customer_id'] = $customer_id;
            $this->load->library('Pagenavi');
            $this->pagenavi->search_data = $search_data;
            $this->pagenavi->per_page = 20;
            $this->pagenavi->base_url = '/admin/customers/docs/' . $customer_id . '/?';
            $this->pagenavi->process($this->adminModel, 'searchCustomerDocs');
            $data['PAGING'] = $this->pagenavi->links_html;
            $data['docs'] = $this->pagenavi->items;
            $this->_iframe("customers/docs", $data);
        }
    }

    public function download_doc($doc_id) {
        if (!empty($doc_id)) {
            $doc = $this->adminModel->getDocById($doc_id);
            if (!empty($doc['doc_id'])) {
                $this->load->helper('download');
                $doc_folder = dirname(DOC_ROOT_PATH) . "/doc_drive/" . $doc['customer_id'] . "/";
                if (file_exists($doc_folder . $doc['doc_path'])) {
                    $doc_data = file_get_contents($doc_folder . $doc['doc_path']);
                    force_download($doc['doc_path'], $doc_data);
                    exit;
                }
            }
        }
    }

}
