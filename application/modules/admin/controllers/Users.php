<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "adminModel", true);
        $this->_admin_check();
    }

    public function index() {
        $data = array();
        if ($this->input->get("act") !== null && $this->input->get("act") == "del") {
            $this->adminModel->delUser($this->input->get("user_id"));
            setMessage("Your user has been deleted successfully!");
            redirect(base_url() . "admin/users/");
        }
        if ($this->input->get("act") !== null && $this->input->get("act") == "status") {
            $is_active = $this->input->get("sta") == "1" ? "1" : "0";
            $this->adminModel->updateUser($this->input->get("user_id"), ["is_active" => $is_active]);
            setMessage("Your user status has been updated successfully!");
            redirect(base_url() . "admin/users/");
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = array();
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/users/?';
        $this->pagenavi->process($this->adminModel, 'searchUsers');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['users'] = $this->pagenavi->items;
        $this->_template("users/index", $data);
    }

    public function add() {
        $data = array();
        if ($this->input->post("email") !== null) {
            $pdata = array();
            $pdata['first_name'] = $this->input->post("first_name");
            $pdata['last_name'] = $this->input->post("last_name");
            $pdata['email'] = $this->input->post("email");
            $pdata['username'] = $this->input->post("email");
            $pdata['password'] = $this->input->post("password");
            $pdata['role_id'] = $this->input->post("role_id");
            $user_id = $this->adminModel->addUser($pdata);
            setMessage("Your user has been added successfully!");
            redirect(base_url() . "admin/users/");
        }
        $this->_template("users/form", $data);
    }

    public function edit() {
        $data = array();
        if ($this->input->post("user_id") !== null && $this->input->post("PROFILE") !== null) {
            $pdata = array();
            $pdata['first_name'] = $this->input->post("first_name");
            $pdata['last_name'] = $this->input->post("last_name");
            $pdata['role_id'] = $this->input->post("role_id");
            $this->adminModel->updateUser($this->input->post("user_id"), $pdata);
            setMessage("Staff has been updated successfully!");
            redirect(base_url() . "admin/users/");
        } else if ($this->input->post("user_id") !== null && $this->input->post("PWD") !== null) {
            $this->adminModel->updateUser($this->input->post("user_id"), [
                "password" => $this->input->post("password")
            ]);
            setMessage("Staff password has been updated");
            redirect(base_url()."admin/users/");
        }
        $data['user'] = $this->adminModel->getUserById($this->input->get("user_id"));
        $this->_template("users/form", $data);
    }

    public function check_email() {
        if (!empty($_GET['email'])) {
            if ($this->adminModel->getUserByEmail($_GET['email']) === false) {
                echo "true";
                exit;
            } else {
                echo "false";
                exit;
            }
        }
        echo "false";
        exit;
    }

}
