<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Leaves extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("LeaveModel", "leaveModel", true);
        $this->_admin_check();
    }

    public function index() {
        $data = array();
        if ($this->input->get("act") !== null && $this->input->get("act") == "del") {
            $this->leaveModel->delLeave($this->input->get("leave_id"));
            setMessage("Your leave has been deleted successfully!");
            redirect(base_url() . "admin/leaves/");
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = array();
        $this->pagenavi->hidden_data = ['user_id' => $_SESSION['USER_ID']];
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/leaves/?';
        $this->pagenavi->process($this->leaveModel, 'searchLeaves');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['leaves'] = $this->pagenavi->items;
        $this->_template("leaves/index", $data);
    }

    public function add() {
        $data = array();
        if ($this->input->post("leave_date") !== null) {
            $pdata = array();
            $pdata['user_id'] = $_SESSION['USER_ID'];
            $pdata['leave_date'] = dateForm2DB($this->input->post("leave_date"));
            $pdata['reason'] = $this->input->post("reason");
            $leave_id = $this->leaveModel->addLeave($pdata);
            setMessage("Your leave has been added successfully!");
            redirect(base_url() . "admin/leaves/");
        }
        $this->_template("leaves/form", $data);
    }

    public function edit() {
        $data = array();
        if ($this->input->post("leave_id") !== null) {
            $pdata = array();
            $pdata['leave_date'] = dateForm2DB($this->input->post("leave_date"));
            $pdata['reason'] = $this->input->post("reason");
            $this->leaveModel->updateLeave($this->input->post("leave_id"), $pdata);
            setMessage("Your leave has been updated successfully!");
            redirect(base_url() . "admin/leaves/");
        }
        $data['leave'] = $this->leaveModel->getLeaveById($this->input->get("leave_id"));
        $this->_template("leaves/form", $data);
    }

}
