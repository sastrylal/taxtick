<div class="container">
    <section class="content-header">
        <h1> Users <small><?php echo!empty($user['user_id']) ? "Edit" : "Add"; ?></small> </h1>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        <div class="box box-info">
            <form id="frm" class="form-horizontal" method="post">
                <input type="hidden" name="PROFILE" id="PROFILE" value="true" />
                <input type="hidden" name="user_id" id="user_id" value="<?php echo!empty($user['user_id']) ? $user['user_id'] : ""; ?>" />
                <div class="box-body">
                    <div class="form-group">
                        <label for="first_name" class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control required" id="first_name" name="first_name" placeholder="First Nmae" value="<?php echo!empty($user['first_name']) ? $user['first_name'] : ""; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control required" id="last_name" name="last_name" placeholder="Last Nmae" value="<?php echo!empty($user['last_name']) ? $user['last_name'] : ""; ?>" />
                        </div>
                    </div>                                     
                    <?php if (!empty($user['user_id'])) { ?>
                    <?php } else { ?>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="email" class="form-control required" id="email" name="email" placeholder="Email" value="<?php echo!empty($user['email']) ? $user['email'] : ""; ?>" />
                            </div>
                        </div>   
                        <!--div class="form-group">
                            <label for="username" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control required" id="username" name="username" placeholder="Username" value="<?php echo!empty($user['username']) ? $user['username'] : ""; ?>" />
                            </div>
                        </div-->
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control required" id="password" name="password" placeholder="Password" value="" />
                            </div>
                        </div> 
                    <?php } ?>
                    <div class="form-group">
                        <label for="role_id" class="col-sm-2 control-label">Role Name</label>
                        <div class="col-sm-6">
                            <select name="role_id" id="role_id" class="form-control required">
                                <option value="STAFF" <?php echo (!empty($user['role_id']) && $user['role_id'] == "STAFF" ) ? 'selected="true"' : ""; ?>>Staff</option>
                                <option value="ADMIN" <?php echo (!empty($user['role_id']) && $user['role_id'] == "ADMIN" ) ? 'selected="true"' : ""; ?>>Administrator</option>
                            </select>                            
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info"><?php echo!empty($user['user_id']) ? "Update" : "Add"; ?></button>
                            <a href="/admin/users/" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="box box-info">
            <form id="frmPwd" class="form-horizontal" method="post">
                <input type="hidden" name="PWD" id="PWD" value="true" />
                <input type="hidden" name="user_id" id="user_id" value="<?php echo!empty($user['user_id']) ? $user['user_id'] : ""; ?>" />
                <div class="box-body">
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-4">
                            <input type="password" class="form-control required" id="password" name="password" placeholder="Password" value="" />
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-4">
                            <input type="password" class="form-control required" id="confirm_password" name="confirm_password" placeholder="Password" value="" />
                        </div>
                    </div>                    
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info"><?php echo!empty($user['user_id']) ? "Update" : "Add"; ?></button>
                            <a href="/admin/profile/" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: "/admin/users/check_email/"
                }
            },
            messages: {
                email: {
                    required: "Please enter email",
                    email: "Please enter valid email",
                    remote: "{0} is already in use, please choose a different email"
                }
            }
        });
        $("#frmPwd").validate({
            rules: {
                password: {
                    minlength: 5
                },
                confirm_password: {
                    minlength: 5,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: "Please enter password"
                },
                confirm_password: {
                    required: "Please enter confirm password"
                },
            }
        });
    });
</script>