<div class="container">
    <section class="content-header">
        <h1>
            Users
        </h1>
        <div class="add">
            <a href="/admin/users/add/" class="btn btn-info pull-right">Add User</a>
        </div>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th width="80px;">#</th>
                    <th width="400px;">Username</th>
                    <th>Email</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Password</th>
                    <th>Role</th>
                    <th width="80px;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($users)) { ?>
                    <?php foreach ($users as $user) { ?>
                        <tr>
                            <td><?php echo $user['user_id']; ?></td>
                            <td><?php echo!empty($user['username']) ? $user['username'] : ""; ?></td>
                            <td><?php echo!empty($user['email']) ? $user['email'] : ""; ?></td>
                            <td><?php echo!empty($user['first_name']) ? $user['first_name'] : ""; ?></td>
                            <td><?php echo!empty($user['last_name']) ? $user['last_name'] : ""; ?></td>
                            <td><?php echo!empty($user['password']) ? $user['password'] : ""; ?></td>
                            <td><?php echo!empty($user['role_name']) ? $user['role_name'] : ""; ?></td>
                            <td>
                                <a href="/admin/users/edit/?user_id=<?php echo $user['user_id']; ?>"
                                   title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="/admin/users/?act=status&user_id=<?php echo $user['user_id']; ?>&sta=<?php echo ($user['is_active'] == "1") ? "0" : "1"; ?>"
                                   title='<?php echo ($user['is_active'] == "1") ? "Active" : "Inactive"; ?>'><i
                                        class="glyphicon glyphicon-star <?php echo ($user['is_active'] == "1") ? "" : "grey"; ?>"></i></a>
                                <a href="/admin/users/?act=del&user_id=<?php echo $user['user_id']; ?>"
                                   title='Delete' onclick="return window.confirm('Do You Want to Delete?');"><i
                                        class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="4">No User(s) found.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo!empty($PAGING) ? $PAGING : ""; ?>
    </div>
</div>