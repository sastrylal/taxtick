<div class="container">
    <section class="content-header">
        <h1>Customers</h1>        
    </section>
    <div class="content">
        <div class="box box-info">            
            <div class="box-body">
                <form class="form-inline"  method="get">
                    <div class="form-group">
                        <label for="key">Key:</label>
                        <input type="text" class="form-control" name="key" id="key" value="<?php echo!empty($_GET['key']) ? $_GET['key'] : ""; ?>" placeholder="Name or email or mobile" />
                    </div>                                       
                    <button class="btn btn-default" id="sendEmail">Search <i class="fa fa-arrow-circle-right"></i></button>                    
                    <a href="/admin/customers/" class="btn btn-default">Reset</a>
                </form>
            </div>
        </div>
        <?php getMessage(); ?>        
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th width="80px;">#</th>                    
                    <th>Email</th>
                    <th>Mobile</th>
                    <th width="140px;">Completed Slots</th>
                    <th width="160px;">Upcoming Slots</th>
                    <th>Doc Drive</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($customers)) { ?>
                    <?php foreach ($customers as $customer) { ?>
                        <tr>
                            <td><?php echo $customer['customer_id']; ?></td>                            
                            <td><a href="/admin/customers/view/?customer_id=<?php echo $customer['customer_id']; ?>" data-remote="false" data-toggle="modal" data-target="#viewModel"><?php echo!empty($customer['email']) ? $customer['email'] : ""; ?></a></td>
                            <td><?php echo!empty($customer['mobile']) ? $customer['mobile'] : ""; ?></td>
                            <td><?php echo!empty($customer['completed_slots']) ? $customer['completed_slots'] : ""; ?></td>
                            <td><?php echo!empty($customer['in_process_slots']) ? $customer['in_process_slots'] : ""; ?></td>
                            <td><a href="/admin/customers/docs/<?php echo $customer['customer_id']; ?>/" class="docfancybox fancybox.iframe">Docs</a></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="6">No Customer(s) found.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo!empty($PAGING) ? $PAGING : ""; ?>
    </div>
</div>

<div id="viewModel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer Info</h4>
            </div>
            <div class="modal-body">

            </div>            
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#viewModel").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".docfancybox").fancybox({
            'width': '90%',
            afterClose: function () {
                window.location = window.location;
            }
        });
    });
    function closeFancybox() {
        $.fancybox.close();
    }
</script>