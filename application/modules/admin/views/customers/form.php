<div class="container">
    <section class="content-header">
        <h1>
            Customers
            <small><?php echo!empty($customer['customer_id']) ? "Edit" : "Add"; ?></small>
        </h1>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        <div class="box box-info">
            <form id="frm" class="form-horizontal" method="post">
                <input type="hidden" name="customer_id" id="customer_id" value="<?php echo!empty($customer['customer_id']) ? $customer['customer_id'] : ""; ?>" />
                <div class="box-body">
                    <div class="form-group">
                        <label for="first_name" class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control required" id="first_name" name="first_name" placeholder="First Nmae" value="<?php echo!empty($customer['first_name']) ? $customer['first_name'] : ""; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control required" id="last_name" name="last_name" placeholder="Last Nmae" value="<?php echo!empty($customer['last_name']) ? $customer['last_name'] : ""; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control required" id="email" name="email" placeholder="Email" value="<?php echo!empty($customer['email']) ? $customer['email'] : ""; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="role_id" class="col-sm-2 control-label">Role Name</label>
                        <div class="col-sm-6">
                            <select name="role_id" id="role_id" class="form-control required">
                                <option value="ADMIN" <?php echo (!empty($customer['role_id']) && $customer['role_id'] == "ADMIN" ) ? 'selected="true"' : ""; ?>>Administrator</option>
                                <option value="SUPER_ADMIN" <?php echo (!empty($customer['role_id']) && $customer['role_id'] == "SUPER_ADMIN" ) ? 'selected="true"' : ""; ?>>Super Administrator</option>
                            </select>                            
                        </div>
                    </div>
                    <?php if (!empty($customer['customer_id'])) { ?>
                    <?php } else { ?>
                        <div class="form-group">
                            <label for="username" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control required" id="username" name="username" placeholder="Question" value="<?php echo!empty($customer['username']) ? $customer['username'] : ""; ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control required" id="password" name="password" placeholder="Question" value="" />
                            </div>
                        </div> 
                    <?php } ?>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info"><?php echo!empty($customer['customer_id']) ? "Update" : "Add"; ?></button>
                            <a href="/admin/users/" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
    });
</script>