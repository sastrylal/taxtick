<div class="content">    
    <table class="table table-striped table-hover table-bordered">
        <tr>
            <th>S.No</th>
            <th>Doc Name</th>
            <th>Password</th>
            <th>Financial Year</th>
            <th>Doc Type</th>
            <th>Date</th>
        </tr>
        <?php if (!empty($docs)) { ?>
            <?php foreach ($docs as $doc) { ?>
                <tr>
                    <td><?php echo $doc['doc_id']; ?></td>
                    <td><a href="/admin/customers/download_doc/<?php echo $doc['doc_id']; ?>/"><?php echo!empty($doc['doc_name']) ? $doc['doc_name'] : ""; ?></a></td>
                    <td><?php echo!empty($doc['doc_password']) ? $doc['doc_password'] : ""; ?></td>
                    <td><?php echo!empty($doc['financial_year']) ? $doc['financial_year'] : ""; ?></td>
                    <td><?php echo!empty($doc['doc_type']) ? $doc['doc_type'] : ""; ?></td>
                    <td><?php echo!empty($doc['created_on']) ? dateTimeDB2SHOW($doc['created_on']) : ""; ?></td>                    
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="7">No document(s) found.</td>
            </tr>
        <?php } ?>                                
    </table>
    <?php echo!empty($PAGING) ? $PAGING : ""; ?>
</div>
