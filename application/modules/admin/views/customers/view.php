<div class="box box-info">
    <div class="box-body">        
        <table class="table">
            <tr>
                <td style="width: 120px;">First Name</td>
                <td>: <?php echo!empty($customer['first_name']) ? $customer['first_name'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 120px;">Last Name</td>
                <td>: <?php echo!empty($customer['last_name']) ? $customer['last_name'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 120px;">Email</td>
                <td>: <?php echo!empty($customer['email']) ? $customer['email'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 120px;">Password</td>
                <td>: <?php echo!empty($customer['password']) ? $customer['password'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 120px;">Mobile</td>
                <td>: <?php echo!empty($customer['mobile']) ? $customer['mobile'] : ""; ?></td>
            </tr>            
        </table>                
    </div>
</div>