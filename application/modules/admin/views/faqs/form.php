<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<div class="container">
    <section class="content-header">
        <h1>
            FAQ's
            <small><?php echo!empty($question['faq_id']) ? "Edit" : "Add"; ?></small>
        </h1>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        <div class="box box-info">
            <form id="frm" class="form-horizontal" method="post">
                <input type="hidden" name="faq_id" id="faq_id" value="<?php echo!empty($question['faq_id']) ? $question['faq_id'] : ""; ?>" />
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control required" id="title" name="title" placeholder="Question" value="<?php echo!empty($question['title']) ? $question['title'] : ""; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-8">
                            <textarea class="form-control required" id="description" name="description" rows="14"><?php echo!empty($question['description']) ? $question['description'] : ""; ?></textarea>
                        </div>
                    </div>                    
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info"><?php echo!empty($question['faq_id']) ? "Update" : "Add"; ?></button>
                            <a href="/admin/faqs/" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
    });
    /*CKEDITOR.config.toolbar = [
        ['Format', 'Font', 'FontSize', '-', 'Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-', 'Print'],
        ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Flash', 'Smiley', 'TextColor', 'BGColor', 'Source']
    ];*/
    var editor = CKEDITOR.replace("description", {
        //filebrowserBrowseUrl: '/js/ckeditor/plugins/imageuploader/imgbrowser.php',
    });
</script>