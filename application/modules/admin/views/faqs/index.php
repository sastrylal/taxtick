<div class="container">
    <section class="content-header">
        <h1>
            FAQ's
        </h1>
        <div class="add">
            <a href="/admin/faqs/add/" class="btn btn-info pull-right">Add Question</a>
        </div>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th width="80px;">#</th>
                    <th width="400px;">Question</th>
                    <th>Description</th>                    
                    <th width="80px;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($questions)) { ?>
                    <?php foreach ($questions as $question) { ?>
                        <tr>
                            <td><?php echo $question['faq_id']; ?></td>
                            <td><?php echo!empty($question['title']) ? $question['title'] : ""; ?></td>
                            <td><?php echo!empty($question['description']) ? $question['description'] : ""; ?></td>
                            <td>
                                <a href="/admin/faqs/edit/?faq_id=<?php echo $question['faq_id']; ?>"
                                   title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="/admin/faqs/?act=status&faq_id=<?php echo $question['faq_id']; ?>&sta=<?php echo ($question['is_active'] == "1") ? "0" : "1"; ?>"
                                   title='<?php echo ($question['is_active'] == "1") ? "Active" : "Inactive"; ?>'><i
                                        class="glyphicon glyphicon-star <?php echo ($question['is_active'] == "1") ? "" : "grey"; ?>"></i></a>
                                <a href="/admin/faqs/?act=del&faq_id=<?php echo $question['faq_id']; ?>"
                                   title='Delete' onclick="return window.confirm('Do You Want to Delete?');"><i
                                        class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="4">No Question(s) found.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo!empty($PAGING) ? $PAGING : ""; ?>
    </div>
</div>