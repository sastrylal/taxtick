<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <title>Admin Panel</title>

        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="/css/admin.css" rel="stylesheet">
        <link href="/fancy/jquery.fancybox.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <script type="text/javascript" src="/fancy/jquery.fancybox.js"></script>   
        <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
    </head>
    <body class="admin">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Admin</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav pull-right">
                        <!--li class=""><a href="/admin/">Dashboard</a></li-->
                        <li class=""><a href="/admin/slots/">New Slots Bookings</a></li>
                        <?php if ($_SESSION['ROLE'] == "ADMIN") { ?>
                            <li class=""><a href="/admin/askus/">Ask Us</a></li>                        
                            <li class=""><a href="/admin/customers/">Customers</a></li>                        
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setup<span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-right">                                
                                    <li><a href="/admin/promos/">Promo Codes</a></li>
                                    <li><a href="/admin/users/">Users</a></li>
                                    <li><a href="/admin/faqs/">Faqs</a></li>
                                </ul>
                            </li>
                        <?php } ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account<span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="/admin/profile/">Profile</a></li>
                                <li><a href="/admin/leaves/">My Leaves</a></li>
                                <li><a href="/admin/schedule/">My Schedule</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/admin/logout/">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="main-container">

