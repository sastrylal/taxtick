<div class="container">
    <section class="content-header">
        <h1>
            Promotional Codes
        </h1>
        <div class="add">
            <a href="/admin/promos/add/" class="btn btn-info pull-right">Add Promo</a>
        </div>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th width="80px;">#</th>
                    <th width="400px;">Promo Code</th>                    
                    <th>Type</th>
                    <th>Discount</th>
                    <th width="80px;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($promos)) { ?>
                    <?php foreach ($promos as $promo) { ?>
                        <tr>
                            <td><?php echo $promo['promo_id']; ?></td>
                            <td><?php echo!empty($promo['promo_code']) ? $promo['promo_code'] : ""; ?></td>
                            <td><?php echo!empty($promo['promo_type']) ? $promo['promo_type'] : ""; ?></td>
                            <td><?php echo!empty($promo['discount']) ? $promo['discount'] : ""; ?><?php echo ($promo['promo_type'] == "Percentage") ? "%" : ""; ?></td>
                            <td>
                                <a href="/admin/promos/edit/?promo_id=<?php echo $promo['promo_id']; ?>"
                                   title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="/admin/promos/?act=status&promo_id=<?php echo $promo['promo_id']; ?>&sta=<?php echo ($promo['is_active'] == "1") ? "0" : "1"; ?>"
                                   title='<?php echo ($promo['is_active'] == "1") ? "Active" : "Inactive"; ?>'><i
                                        class="glyphicon glyphicon-star <?php echo ($promo['is_active'] == "1") ? "" : "grey"; ?>"></i></a>
                                <!--a href="/admin/promos/?act=del&promo_id=<?php echo $promo['promo_id']; ?>"
                                   title='Delete' onclick="return window.confirm('Do You Want to Delete?');"><i
                                        class="glyphicon glyphicon-trash"></i></a-->
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="5">No Promo(s) found.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo!empty($PAGING) ? $PAGING : ""; ?>
    </div>
</div>