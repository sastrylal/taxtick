<div class="container">
    <?php getMessage(); ?>
    <form name="frm" id="frm" method="post" enctype="multipart/form-data">
        <div class="user-title">
            <input type="hidden" name="ask_id" value="<?php echo!empty($ask['ask_id']) ? $ask['ask_id'] : ""; ?>" />
            <input type="hidden" name="ASKMSG" value="true" />            
            <?php if (!empty($messages)) { ?>
                <?php foreach ($messages as $message) { ?>
                    <p class="<?php echo ($message['message_by'] == "Customer") ? 'query' : 'answer'; ?>">
                        <?php echo!empty($message['message']) ? nl2br($message['message']) : ""; ?>
                        <?php if (!empty($message['attach_path'])) { ?>
                            <br/> 
                            <span><a href="/data/ask/<?php echo $message['attach_path']; ?>" target="_blank"><?php echo $message['attach_name']; ?></a></span>                            
                        <?php } ?>
                    <div class="pull-right small"><?php echo!empty($message['created_on']) ? dateTimeDB2SHOW($message['created_on']) : ""; ?></div>
                    </p>
                <?php } ?>
            <?php } ?>
            <div class="form-group">
                <label for="message">Answer</label>
                <textarea name="message" id="message" class=" form-control required" required="true" placeholder="Message"></textarea>
            </div>
            <div class="form-group">
                <label for="attach">Attachment</label>
                <input type="file" name="attach" id="attach" class="" />
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-info">Submit</button>                
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
    });
</script>