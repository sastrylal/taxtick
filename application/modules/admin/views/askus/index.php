<div class="container">
    <section class="content-header">
        <h1>Ask Us</h1>        
    </section>
    <div class="content">
        <div class="box box-info">            
            <div class="box-body">
                <form class="form-inline"  method="get">
                    <div class="form-group">
                        <label for="key">Key:</label>
                        <input type="text" class="form-control" name="key" id="key" value="<?php echo!empty($_GET['key']) ? $_GET['key'] : ""; ?>" />
                    </div>
                    <div class="form-group">
                        <select name="assign" class="form-control">
                            <option value="">Assign to</option>
                            <?php if (!empty($users)) { ?>
                                <?php foreach ($users as $sKey => $user) { ?>
                                    <option value="<?php echo $sKey; ?>" <?php echo (!empty($_GET['assign']) && $_GET['assign'] == $sKey ) ? 'selected="true"' : ""; ?>><?php echo $user; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <button class="btn btn-default" id="sendEmail">Search <i class="fa fa-arrow-circle-right"></i></button>                    
                    <a href="/admin/askus/" class="btn btn-default">Reset</a>
                </form>
            </div>
        </div>
        <?php getMessage(); ?>        
        <table class="table table-striped table-hover table-bordered" id="ask_table">
            <thead>
                <tr>
                    <th width="60px;">#</th>
                    <th style="max-width: 240px; width: 220px;">Customer</th>
                    <th>Question</th>
                    <th>Assigned To</th>
                    <th width="120px;">Status</th>
                    <th width="160px;">Created On</th>                    
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($asks)) { ?>
                    <?php foreach ($asks as $ask) { ?>
                        <tr>
                            <td><?php echo $ask['ask_id']; ?></td>
                            <td><?php echo!empty($ask['customer_name']) ? $ask['customer_name'] : ""; ?></td>
                            <td><a href="/admin/askus/ask_messages/<?php echo $ask['ask_id']; ?>/" class="fancybox fancybox.iframe"><?php echo!empty($ask['title']) ? $ask['title'] : ""; ?></a></td>
                            <td class="slot_assign_td" style="width: 160px;">
                                <?php if ($_SESSION['ROLE'] == "ADMIN") { ?>
                                    <span class="slot_assign_label"><?php echo!empty($ask['user_name']) ? $ask['user_name'] : ""; ?></span>
                                    <select name="assign_to" class="form-control slot_assign_sel" data-ask_id="<?php echo $ask['ask_id']; ?>" style="display: none;">
                                        <option value="">None</option>
                                        <?php if (!empty($users)) { ?>
                                            <?php foreach ($users as $sKey => $user) { ?>
                                                <option value="<?php echo $sKey; ?>" <?php echo (!empty($ask['assign_to']) && $ask['assign_to'] == $sKey ) ? 'selected="true"' : ""; ?>><?php echo $user; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>   
                                <?php } else { ?>
                                    <span><?php echo!empty($ask['user_name']) ? $ask['user_name'] : ""; ?></span>
                                <?php } ?>
                            </td>
                            <td><?php echo!empty($ask['status']) ? $ask['status'] : ""; ?></td>
                            <td><?php echo!empty($ask['created_on']) ? dateTimeDB2SHOW($ask['created_on']) : ""; ?></td>                            
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="6">No Ask(s) found.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo!empty($PAGING) ? $PAGING : ""; ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".fancybox").fancybox({
            afterClose: function () {
                window.location = window.location;
            }
        });
<?php if ($_SESSION['ROLE'] == "ADMIN") { ?>
            $("#ask_table td.slot_assign_td").on("click", function () {
                $(this).find(".slot_assign_label").hide();
                $(this).find(".slot_assign_sel").show();
            });
            $("#ask_table select.slot_assign_sel").on("change", function () {
                var slot_assign_sel = $(this);
                $.ajax({
                    url: "/admin/askus/change_assign_to/",
                    type: "POST",
                    data: {"ask_id": $(this).data("ask_id"), "assign_to": $(this).val()},
                    success: function (data) {
                        slot_assign_sel.hide(20, function () {
                            slot_assign_sel.prev(".slot_assign_label").show(10);
                            slot_assign_sel.hide();
                        });
                    }
                });
                $(this).prev(".slot_assign_label").html($("option:selected", this).text());
            });
<?php } ?>
    });
    function closeFancybox() {
        $.fancybox.close();
    }
</script>