<div class="container">
    <section class="content-header">
        <h1>Leaves</h1>
        <div class="add">
            <a href="/admin/leaves/add/" class="btn btn-info pull-right">Add Leave</a>
        </div>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th width="80px;">#</th>
                    <th width="400px;">Leave Date</th>
                    <th>Reason</th>                    
                    <th width="80px;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($leaves)) { ?>
                    <?php foreach ($leaves as $leave) { ?>
                        <tr>
                            <td><?php echo $leave['leave_id']; ?></td>
                            <td><?php echo!empty($leave['leave_date']) ? dateDB2SHOW($leave['leave_date']) : ""; ?></td>
                            <td><?php echo!empty($leave['reason']) ? $leave['reason'] : ""; ?></td>
                            <td>
                                <!--a href="/admin/leaves/edit/?leave_id=<?php echo $leave['leave_id']; ?>" title='Edit'><i class="glyphicon glyphicon-edit"></i></a-->
                                <a href="/admin/leaves/?act=del&leave_id=<?php echo $leave['leave_id']; ?>" title='Delete' onclick="return window.confirm('Do You Want to Delete?');"><i class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="4">No Leave(s) found.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo!empty($PAGING) ? $PAGING : ""; ?>
    </div>
</div>