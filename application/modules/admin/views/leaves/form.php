<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script> 
<div class="container">
    <section class="content-header">
        <h1>
            Leave
            <small><?php echo!empty($leave['leave_id']) ? "Edit" : "Add"; ?></small>
        </h1>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        <div class="box box-info">
            <form id="frm" class="form-horizontal" method="post">
                <input type="hidden" name="leave_id" id="leave_id" value="<?php echo!empty($leave['leave_id']) ? $leave['leave_id'] : ""; ?>" />
                <div class="box-body">
                    <div class="form-group">
                        <label for="leave_date" class="col-sm-2 control-label">Leave Date</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control required" id="leave_date" name="leave_date" value="<?php echo!empty($leave['leave_date']) ? dateDB2SHOW($leave['leave_date']) : ""; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="reason" class="col-sm-2 control-label">Reason</label>
                        <div class="col-sm-4">
                            <textarea class="form-control required" id="reason" name="reason"><?php echo!empty($leave['reason']) ? $leave['reason'] : ""; ?></textarea>
                        </div>
                    </div>                    
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info"><?php echo!empty($leave['leave_id']) ? "Update" : "Add"; ?></button>
                            <a href="/admin/leaves/" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
        $('#leave_date').datepicker({
            format: "dd/mm/yyyy",
            startDate: '0',
            endDate: '+7d'
        });
    });    
</script>