<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Login</title>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/admin.css" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="about-wrapper">
        <div class="container">
            <h2 class="form-signin-heading">Login</h2>
            <div class="form_container">
            <form id="frm" class="form-signin" method="post">
                <ul>
                    <li>
                        <label for="username" class="sr-only">Username</label>
                        <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus/>
                        </li>
                        <li>
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required/>                
                            </li>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button></li>
                </ul>
            </form>
        </div></div>
            </div>
    </body>
</html>