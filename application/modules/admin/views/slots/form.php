<div class="container">
    <section class="content-header">
        <h1>  Slots <small><?php echo!empty($slot['slot_id']) ? "Edit" : "Add"; ?></small></h1>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        <div class="box box-info">
            <form id="frm" class="form-horizontal" method="post">
                <input type="hidden" name="slot_id" id="slot_id" value="<?php echo!empty($slot['slot_id']) ? $slot['slot_id'] : ""; ?>" />
                <div class="box-body">
                    <div class="form-group">
                        <label for="customer_name" class="col-sm-2 control-label">Customer Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control required" id="customer_name" name="customer_name" placeholder="Customer Nmae" value="<?php echo!empty($slot['customer_name']) ? $slot['customer_name'] : ""; ?>" />
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control required" id="email" name="email" placeholder="Email" value="<?php echo!empty($slot['email']) ? $slot['email'] : ""; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control required" id="mobile" name="mobile" placeholder="Email" value="<?php echo!empty($slot['mobile']) ? $slot['mobile'] : ""; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sharing_software" class="col-sm-2 control-label">Sharing Software</label>
                        <div class="col-sm-6">
                            <select name="sharing_software" id="sharing_software" class="form-control required">
                                <?php if (!empty($sharing_softwares)) { ?>
                                    <?php foreach ($sharing_softwares as $sKey => $software) { ?>
                                        <option value="<?php echo $sKey; ?>" <?php echo (!empty($slot['sharing_software']) && $slot['sharing_software'] == $sKey ) ? 'selected="true"' : ""; ?>><?php echo $software; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="slot_date" class="col-sm-2 control-label">Slot Date</label>
                        <div class="col-sm-6">
                            <?php echo !empty($slot['slot_date'])?dateTimeDB2SHOW($slot['slot_date']):""; ?>
                            <!--select name="slot_date" id="slot_date" class="form-control required">
                                <option value="">-Select-</option>
                            <?php /*if (!empty($available_slot_dates)) { ?>
                                <?php foreach ($available_slot_dates as $slot_date) { ?>
                                                <option value="<?php echo $slot_date; ?>" <?php echo (!empty($slot['slot_date']) && $slot['slot_date'] == "ADMIN" ) ? 'selected="true"' : ""; ?> ><?php echo $slot_date; ?></option>                                    
                                <?php } ?>
                            <?php }*/ ?>
                            </select-->                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="assign_to" class="col-sm-2 control-label">Assign To</label>
                        <div class="col-sm-6">
                            <select name="assign_to" id="assign_to" class="form-control required">
                                <?php if (!empty($users)) { ?>
                                    <?php foreach ($users as $sKey => $user) { ?>
                                        <option value="<?php echo $sKey; ?>" <?php echo (!empty($slot['assign_to']) && $slot['assign_to'] == $sKey ) ? 'selected="true"' : ""; ?>><?php echo $user; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>                            
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info"><?php echo!empty($slot['slot_id']) ? "Update" : "Add"; ?></button>
                            <a href="/admin/slots/" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
    });
</script>