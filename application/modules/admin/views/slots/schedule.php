<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script> 
<?php getMessage(); ?>
<div class="box box-info" style="vertical-align: middle; height: 200px;">
    <form id="frm" class="form-horizontal" method="post">
        <input type="hidden" name="slot_id" id="slot_id" value="<?php echo!empty($slot['slot_id']) ? $slot['slot_id'] : ""; ?>" />
        <div class="box-body">
            <div class="form-group">
                <label for="slot_date" class="col-xs-3 control-label">Current Slot Date</label>
                <div class="col-xs-4">
                    <?php echo!empty($slot['slot_date']) ? dateTimeDB2SHOW($slot['slot_date']) : ""; ?>
                </div>
            </div>
            <div class="form-group">
                <label for="slot_date" class="col-xs-3 control-label">Re-Schedule</label>
                <div class="col-xs-4">
                    <div class="input-group date" data-provide="">
                        <input type="text" name="slot_date" id="slot_date" class="form-control required" placeholder="Select date" required="true">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="slot_date" class="col-xs-3 control-label">Time</label>
                <div class="col-xs-4">
                    <select class="form-control required" name="slot_time" id="slot_time" required="true">
                    </select>
                </div>                
            </div>
        </div>
        <div class="box-footer">
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <button type="submit" class="btn btn-info">Schedule</button>
                    <a href="/admin/slots/schedule/<?php echo!empty($slot['slot_id']) ? $slot['slot_id'] : ""; ?>/" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#slot_date').datepicker({
            format: "dd/mm/yyyy",
            startDate: '0',
            endDate: '+7d'
        });
        $('#slot_date').on('change', function () {
            $.ajax({
                url: "/admin/slots/available_slot_times/",
                type: "POST",
                dataType: 'json',
                data: {"slot_date": $('#slot_date').val()},
                success: function (data) {
                    $("#slot_time").html("");
                    $.each(data, function (i, item) {
                        $('#slot_time').append($('<option>', {
                            value: i,
                            text: item
                        }));
                    });
                }
            });
        });
    });
</script>