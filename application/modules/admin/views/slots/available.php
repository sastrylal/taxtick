<div class="container">
    <section class="content-header">
        <h1> Slot Available</h1>
    </section>
    <div class="content slotbooking">        
        <?php getMessage(); ?>        
        <table class="table table-striped table-hover table-bordered" id="slot_tabel">
            <thead>
                <tr>
                    <th width="80px;">#</th>                    
                    <th>Date Time</th>                    
                    <th>Slot Max Bookings</th>                    
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($slots)) { ?>
                    <?php foreach ($slots as $slot) { ?>
                        <tr>
                            <td><?php echo $slot['slot_id']; ?></td>
                            <td>
                                <a href="/admin/slots/schedule/<?php echo $slot['slot_id']; ?>/" class="fancybox fancybox.iframe">
                                    <?php echo!empty($slot['slot_date']) ? dateTimeDB2SHOW($slot['slot_date']) : ""; ?>
                                </a>
                                <?php if (!empty($slot['reschedule'])) { ?>
                                    <a href="#" data-remote="false" data-toggle="modal" data-target="#reschedule_content">(R)</a>
                                    <div style="display: none;" id="reschedule_content<?php echo $slot['slot_id']; ?>"><?php echo $slot['reschedule']; ?></div>
                                <?php } ?>
                            </td>
                            <td>
                                <a href="/admin/slots/view/<?php echo $slot['slot_id']; ?>/" data-remote="false" data-toggle="modal" data-target="#viewModel">
                                    <?php echo!empty($slot['customer_name']) ? $slot['customer_name'] : ""; ?>
                                </a>
                            </td>
                            <td><?php echo!empty($slot['email']) ? $slot['email'] : ""; ?></td>
                            <td><?php echo!empty($slot['mobile']) ? $slot['mobile'] : ""; ?></td>                            
                            <td class="slot_status_td" style="width: 130px;">
                                <span class="slot_status_label"><?php echo!empty($slot['slot_status']) ? $slot['slot_status'] : ""; ?></span>
                                <select name="status" class="form-control slot_status_sel" data-slot_id="<?php echo $slot['slot_id']; ?>" style="display: none;">
                                    <option value=""></option>
                                    <option value="In Process" <?php echo (!empty($slot['slot_status']) && $slot['slot_status'] == "In Process") ? 'selected="true"' : ""; ?>>In Process</option>
                                    <option value="Completed" <?php echo (!empty($slot['slot_status']) && $slot['slot_status'] == "Completed") ? 'selected="true"' : ""; ?>>Completed</option>
                                </select>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="9">No Slot(s) found.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo!empty($PAGING) ? $PAGING : ""; ?>
    </div>
</div>
<div id="viewModel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Slot Info</h4>
            </div>
            <div class="modal-body">

            </div>            
        </div>
    </div>
</div>
<div id="reschedule_content" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Slot Reschedule</h5>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#viewModel").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
    $("#reschedule_content").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").html(link.next().html());
    });
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy"
        });
        $(".fancybox").fancybox({
            afterClose: function () {
                window.location = window.location;
            }
        });
<?php if ($_SESSION['ROLE'] == "ADMIN") { ?>
            $("#slot_tabel td.slot_assign_td").on("click", function () {
                $(this).find(".slot_assign_label").hide();
                $(this).find(".slot_assign_sel").show();
            });
            $("#slot_tabel select.slot_assign_sel").on("change", function () {
                var slot_assign_sel = $(this);
                $.ajax({
                    url: "/admin/slots/change_assign_to/",
                    type: "POST",
                    data: {"slot_id": $(this).data("slot_id"), "assign_to": $(this).val()},
                    success: function (data) {
                        slot_assign_sel.hide(20, function () {
                            slot_assign_sel.prev(".slot_assign_label").show(10);
                            slot_assign_sel.hide();
                        });
                    }
                });
                $(this).prev(".slot_assign_label").html($("option:selected", this).text());
            });
<?php } ?>
        $("#slot_tabel td.slot_status_td").on("click", function () {
            $(this).find(".slot_status_label").hide();
            $(this).find(".slot_status_sel").show();
        });
        $("#slot_tabel select.slot_status_sel").on("change", function () {
            var slot_status_sel = $(this);
            $.ajax({
                url: "/admin/slots/change_status/",
                type: "POST",
                //dataType: 'json',
                data: {"slot_id": $(this).data("slot_id"), "slot_status": $(this).val()},
                success: function (data) {
                    slot_status_sel.hide(20, function () {
                        slot_status_sel.prev(".slot_status_label").show(10);
                        slot_status_sel.hide();
                    });
                }
            });
            $(this).prev(".slot_status_label").html($(this).val());
        });
        $("select.slot_status_sel").hide();
    });
    function closeFancybox() {
        $.fancybox.close();
    }
</script>