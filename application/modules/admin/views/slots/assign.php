<?php getMessage(); ?>
<div class="box box-info" style="vertical-align: middle; height: 200px;">
    <form id="frm" class="form-horizontal" method="post">
        <input type="hidden" name="slot_id" id="slot_id" value="<?php echo!empty($slot['slot_id']) ? $slot['slot_id'] : ""; ?>" />
        <div class="box-body">
            <div class="form-group">
                <label for="assign_to" class="col-xs-3 control-label">Assign To</label>
                <div class="col-xs-4">
                    <select name="assign_to" id="assign_to" class="form-control required">
                        <option value="">None</option>
                        <?php if (!empty($users)) { ?>
                            <?php foreach ($users as $sKey => $user) { ?>
                                <option value="<?php echo $sKey; ?>" <?php echo (!empty($slot['assign_to']) && $slot['assign_to'] == $sKey ) ? 'selected="true"' : ""; ?>><?php echo $user; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>                            
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <button type="submit" class="btn btn-info">Assign</button>
                    <a href="/admin/slots/assign/<?php echo!empty($slot['slot_id']) ? $slot['slot_id'] : ""; ?>/" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>