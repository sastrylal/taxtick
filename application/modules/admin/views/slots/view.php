<div class="box box-info">
    <div class="box-body">        
        <table class="table">            
            <tr>
                <td style="width: 140px;">Slot Date</td>
                <td>: <?php echo!empty($slot['slot_date']) ? dateTimeDB2SHOW($slot['slot_date']) : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Customer Name</td>
                <td>: <?php echo!empty($slot['customer_name']) ? $slot['customer_name'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Email</td>
                <td>: <?php echo!empty($slot['email']) ? $slot['email'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Mobile</td>
                <td>: <?php echo!empty($slot['mobile']) ? $slot['mobile'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Purpose</td>
                <td>: <?php echo!empty($slot['purpose']) ? $slot['purpose'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Sharing Software</td>
                <td>: <?php echo!empty($slot['sharing_software']) ? $slot['sharing_software'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Promo Code</td>
                <td>: <?php echo!empty($slot['promo_code']) ? $slot['promo_code'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Slot Status</td>
                <td>: <?php echo!empty($slot['slot_status']) ? $slot['slot_status'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Amount</td>
                <td>: <?php echo!empty($slot['amount']) ? $slot['amount'] : ""; ?></td>
            </tr>
            <tr>
                <td style="width: 140px;">Assign To</td>
                <td>: <?php echo!empty($slot['user_name']) ? $slot['user_name'] : ""; ?></td>
            </tr>
        </table>                
    </div>
</div>

