<link href="/css/bootstrap-switch.css" rel="stylesheet">
<script src="/js/bootstrap-switch.js"></script>
<div class="container">
    <section class="content-header">
        <h1> My Schedule Settings</h1>
    </section>
    <div class="content">
        <?php getMessage(); ?>
        <div class="box box-info">
            <form id="frm" class="form-horizontal" method="post">
                <div class="box-body">
                    <?php foreach ($schedule_days as $day) { ?>
                        <div class="form-group">
                            <label for="week_days" class="col-sm-2 control-label"><?php echo $day['day_name']; ?></label>
                            <div class="col-sm-2">
                                <input type="hidden" name="week_days[<?php echo $day['day_id']; ?>]" value="No" />
                                <input type="checkbox" class="checkbox" data-size="small" name="week_days[<?php echo $day['day_id']; ?>]" value="Yes" <?php echo (!empty($day['is_working']) && $day['is_working'] == "Yes") ? 'checked="true"' : ""; ?> />
                            </div>                            
                        </div>
                    <?php } ?>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info">Save</button>
                            <a href="/admin/schedule/" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".checkbox").bootstrapSwitch();
        $("#frm").validate({
            rules: {
                promo_code: {
                    required: true,
                    remote: "/admin/promos/check_promo_code/"
                }
            },
            messages: {
                promo_code: {
                    required: "Please enter promo code",
                    remote: "{0} is already in use"
                }
            }
        });
    });
</script>