<?php
class DBModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    function getSlotByTime() {
        $now = new DateTime();
        $now->modify("+25 minutes");
        $slot_start = $now->format("Y-m-d H:i:s");
        $now->modify("+10 minutes");
        $slot_end = $now->format("Y-m-d H:i:s");
        $this->db->select("m.*");
        $this->db->where("m.slot_date BETWEEN '".$slot_start."' AND '".$slot_end."'");
        $query = $this->db->get("tbl_slots m");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }
    function addCustomer($pdata) {
        $this->db->set("created_on", "NOW()", false);
        $this->db->insert("tbl_customers", $pdata);
        return $this->db->insert_id();
    }
    function updateCustomerByPk($customer_id, $pdata) {
        $this->db->where("customer_id", $customer_id);
        return $this->db->update("tbl_customers", $pdata);
    }
    function getCustomerByPk($customer_id) {
        $this->db->select("m.*");
        $this->db->where("customer_id", $customer_id);
        $query = $this->db->get("tbl_customers m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
    
    function loginCustomer($email, $password) {
        $this->db->select("*");
        $this->db->where("email", $email);
        $this->db->where("password", $password);
        $query = $this->db->get("tbl_customers");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
    
    function getCustomerByEmail($email) {
        $this->db->select("m.*");
        $this->db->where("m.email", $email);
        $query = $this->db->get("tbl_customers m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
    
    function getCustomerByResetToken($token) {
        $this->db->select("m.*");
        $this->db->where("m.reset_token", $token);
        $query = $this->db->get("tbl_customers m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
    
    function getSharingSoftwares(){
        return [
            "Teamviewer" => "Teamviewer",
            "Gtalk" => "Gtalk",
            "Skype" => "Skype"
        ];
    }
}
?>