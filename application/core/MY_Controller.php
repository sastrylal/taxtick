<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {

    public $header_data = [];

    function __construct() {
        parent::__construct();
    }

    function _admin_check() {
        if (!empty($_SESSION['USER_ID'])) {
            
        } else {
            redirect(base_url() . "admin/login/");
        }
    }

    function _customer_check() {
        if (!empty($_SESSION['CUSTOMER_ID'])) {
            
        } else {
            redirect(base_url() . "login/");
        }
    }

    public function sendSMS($to, $msg) {
        $url = "http://sms.scubedigi.com/api.php?username=taxtick1&password=Taxtick@123&to=".$to."&from=TaxTic&message=" . urlencode($msg);    //Store data into URL variable
        $response = file($url);    //Call Url variable by using file() function
        return $response;
    }

    public function sendEmail($view, $data = []) {
        if (empty($data['from'])) {
            $data['from'] = "no-reply@taxtick.in";
            $data['from_name'] = "";
        }
        include_once(rtrim(APPPATH, "/") . "/third_party/phpmailer/class.phpmailer.php");
        $body = $this->load->view($view, $data, true);
        $mail = new PHPMailer();
        if (!empty($data['from']) && !empty($data['from_name'])) {
            $mail->SetFrom($data['from'], $data['from_name']);
        } else if (!empty($data['from'])) {
            $mail->SetFrom($data['from']);
        }
        if (!empty($data['to']) && !empty($data['to_name'])) {
            $mail->AddAddress($data['to']);
        } else if (!empty($data['to'])) {
            $mail->AddAddress($data['to']);
        }
        $mail->Subject = !empty($data['subject']) ? $data['subject'] : "";
        $mail->isHTML(true);
        //$mail->MsgHTML($body);
        $mail->Body = $body;
        $mail->Send();
    }

    public function _template($page_name = 'index', $data = array()) {
        $this->load->view('header', $this->header_data);
        $this->load->view($page_name, $data);
        $this->load->view('footer');
    }

    public function _iframe($page_name = 'index', $data = array()) {
        $this->load->view('iframe_header', $this->header_data);
        $this->load->view($page_name, $data);
        $this->load->view('iframe_footer');
    }

}
