<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slotcron extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("DBModel", "dbModel", true);
    }

    public function index() {
        $this->slotremainder();
    }

    public function slotremainder() {        
        $slots = $this->dbModel->getSlotByTime();
        if (!empty($slots)) {
            foreach ($slots as $slot) {
                $slot_date_time = new DateTime($slot['slot_date']);
                $slot['slot_date_time'] = $slot_date_time;
                $slot['to'] = $slot['email'];
                $slot['subject'] = "Taxtick.in - Slot booking";
                $this->sendEmail("email/slot_30min_remainder", $slot);
                $this->sendSMS($slot['mobile'], "Dear " . $slot['customer_name'] . ", this is to inform you that your scheduled slot is inbound 30 min from now (" . $slot_date_time->format("d-m-Y") . " at " . $slot_date_time->format("G:iA") . ") – www.taxtick.in");
                echo $slot['slot_id'] . " " . $slot['slot_date'] . " " . $slot['customer_name'] . "<br/> \n";
            }
        }
        exit;
    }

}
