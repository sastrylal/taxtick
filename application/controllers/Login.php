<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("DBModel", "dbModel");
    }
    public function index() {        
        $data = [];
        if (!empty($_POST['email'])) {
            $customer = $this->dbModel->loginCustomer($_POST['email'], $_POST['password']);
            if (!empty($customer['customer_id']) && $customer['is_active'] == "1") {
                $_SESSION['NAME'] = !empty($customer['first_name']) ? $customer['first_name'] : (!empty($customer['email']) ? $customer['email'] : "");
                $_SESSION['CUSTOMER_ID'] = $customer['customer_id'];
                redirect(base_url() . "my/");
            }else if($customer['is_active'] == "0"){
                setError("Your account is not activated.");
            }else{
                setError("Invalid email or password.");
            }
        }
        $this->_template("login", $data);
    }
}
