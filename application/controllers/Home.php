<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("DBModel", "dbModel", true);
    }

    public function index() {
        if($this->input->is_cli_request()){
           echo "I am here"; exit; 
        }
        $data = [];
        if (!empty($_POST['CONTACT_US'])) {
            $data = $_POST;
            $data['to'] = 'info@taxtick.in';
            $data['subject'] = "Contact us";
            $this->sendEmail("email/admin_contactus", $data);
            redirect(base_url());
        }
        $this->load->view("home", $data);
    }

    public function signup() {
        $data = [];
        if (!empty($_POST['email'])) {
            $pdata = array();
            $pdata['email'] = !empty($_POST['email']) ? $_POST['email'] : "";
            $pdata['mobile'] = !empty($_POST['mobile']) ? trim($_POST['mobile']) : "";
            $pdata['password'] = !empty($_POST['password']) ? $_POST['password'] : "";
            $email_data = explode("@", $pdata['email']);
            $pdata['first_name'] = !empty($email_data[0]) ? $email_data[0] : "";
            $customer_id = $this->dbModel->addCustomer($pdata);

            $pdata['to'] = $pdata['email'];
            $pdata['subject'] = "Welcome to India's only online Income Tax Assistant www.tacktick.in";
            $this->sendEmail("email/signup", $pdata);
            $this->sendSMS($pdata['mobile'], "Thank you for registering with India's only online Income Tax Assistant www.tacktick.in. If you haven’t registered, report us on +91 903-008-3030");
            setMessage("Your registion has been completed.");
            $customer = $this->dbModel->getCustomerByPk($customer_id);
            if (!empty($customer['customer_id']) && $customer['is_active'] == "1") {
                $_SESSION['NAME'] = !empty($customer['first_name']) ? $customer['first_name'] : (!empty($customer['email']) ? $customer['email'] : "");
                $_SESSION['CUSTOMER_ID'] = $customer['customer_id'];
                redirect(base_url() . "my/");
            }
            redirect(base_url() . "login/");
        }
        $this->_template("signup", $data);
    }

    public function validemail() {
        if (!empty($_GET['email'])) {
            if ($this->dbModel->getCustomerByEmail($_GET['email']) === false) {
                echo "true";
                exit;
            } else {
                echo "false";
                exit;
            }
        }
        echo "false";
        exit;
    }

    public function forgotten() {
        $data = [];
        if (!empty($_POST['email'])) {
            $customer = $this->dbModel->getCustomerByEmail($_POST['email']);
            if (!empty($customer['customer_id'])) {
                $token = md5($customer['customer_id'] . "_" . date("YmdHis"));
                $expairy = new DateTime();
                $expairy->modify("+3 days");
                $this->dbModel->updateCustomerByPk($customer['customer_id'], [
                    "reset_token" => $token,
                    "reset_expairy" => $expairy->format("Y-m-d H:i:s")
                ]);
                $data = $customer;
                $data['to'] = $customer['email'];
                $data['token'] = $token;
                $data['to'] = $customer['email'];
                $data['subject'] = "Login credentials for www.taxtick.in";
                $this->sendEmail("email/forgotten", $data);
                $this->sendSMS($customer['mobile'], "Please check your registered E-mail ID for password. If you haven't requested, immediately report to +91 903-008-3030 – www.taxtick.in");
                setMessage("Reset password link has been sent to your email.");
                redirect(base_url() . "forgotten/");
            } else {
                setError("Invalid email address");
            }
        }
        $this->_template("forgotten", $data);
    }

    public function reset() {
        $data = [];
        if (!empty($_POST['token']) && !empty($_POST['password']) && !empty($_POST['confirm_password'])) {
            if ($_POST['password'] == $_POST['confirm_password']) {
                $customer = $this->dbModel->getCustomerByResetToken($_GET['token']);
                if (!empty($customer['customer_id'])) {
                    $this->dbModel->updateCustomerByPk($customer['customer_id'], [
                        "password" => $_POST['password'],
                        "reset_token" => "",
                        "reset_expairy" => ""
                    ]);
                    setMessage("Your new password has been updated.");
                    redirect(base_url() . "login/");
                }
            } else {
                setError("Confirm password must same as password!");
            }
        } else if (!empty($_GET['token'])) {
            $customer = $this->dbModel->getCustomerByResetToken($_GET['token']);
            if (!empty($customer['customer_id'])) {
                $data['customer'] = $customer;
            } else {
                setError("Invalid token or token already expaired.");
                redirect(base_url() . "forgotten");
            }
        }
        $this->_template("reset", $data);
    }

    function terms_conditions() {
        $this->_template("terms_conditions");
    }

}
