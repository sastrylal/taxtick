-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 12, 2016 at 07:43 AM
-- Server version: 5.5.50-cll
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `taxtick_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_askus`
--

CREATE TABLE IF NOT EXISTS `tbl_askus` (
  `ask_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `assign_to` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`ask_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_askus`
--

INSERT INTO `tbl_askus` (`ask_id`, `customer_id`, `title`, `assign_to`, `created_on`, `status`) VALUES
(1, 1, 'Message 001', 0, '2016-06-26 15:28:22', 'Awaiting Reply'),
(2, 1, 'message 002', 0, '2016-06-26 15:30:58', 'Awaiting Reply'),
(3, 1, 'Message 003', 0, '2016-06-26 15:59:34', 'Replied'),
(4, 6, '201 messgae ', 0, '2016-06-26 16:19:33', 'Awaiting Reply'),
(5, 3, 'Sample', 0, '2016-06-29 22:10:30', 'Replied'),
(6, 10, 'Test 1', 0, '2016-07-04 03:35:22', 'Replied'),
(7, 1, '', 0, '2016-07-07 23:11:23', 'Awaiting Reply'),
(8, 1, 'new test message', 0, '2016-07-08 00:02:49', 'Awaiting Reply'),
(9, 1, 'test', 0, '2016-07-08 00:39:13', 'Awaiting Reply'),
(10, 10, 'abs', 0, '2016-07-08 00:39:16', 'Awaiting Reply'),
(11, 1, 'Time test', 0, '2016-07-08 01:08:55', 'Awaiting Reply'),
(12, 1, 'again time test', 0, '2016-07-08 13:42:00', 'Replied'),
(13, 10, 'Test 2', 4, '2016-07-12 18:28:48', 'Replied'),
(14, 1, 'email test', 4, '2016-07-15 11:42:14', 'Awaiting Reply');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ask_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_ask_messages` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `ask_id` int(11) NOT NULL,
  `message_by` enum('Customer','Taxtick') NOT NULL,
  `message` text NOT NULL,
  `attach_name` varchar(100) NOT NULL,
  `attach_path` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `tbl_ask_messages`
--

INSERT INTO `tbl_ask_messages` (`msg_id`, `ask_id`, `message_by`, `message`, `attach_name`, `attach_path`, `user_id`, `created_on`, `updated_on`) VALUES
(3, 3, 'Customer', 'Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 Message 003 ', 'doc3.pdf', '', 0, '2016-06-26 15:59:34', '0000-00-00 00:00:00'),
(6, 3, 'Customer', 'ge 003 Message 003 Message 003 Message 003 Mes ', '', '', 0, '2016-06-26 16:14:04', '0000-00-00 00:00:00'),
(7, 3, 'Customer', 'Attach test', 'bang_prem.pdf', 'doc7.pdf', 0, '2016-06-26 16:15:48', '0000-00-00 00:00:00'),
(8, 2, 'Customer', 'My messaege', '', '', 0, '2016-06-26 16:16:43', '0000-00-00 00:00:00'),
(9, 1, 'Customer', 'Other message', '', '', 0, '2016-06-26 16:16:52', '0000-00-00 00:00:00'),
(10, 4, 'Customer', 'A asdf ASF SADF ASDF ASDFAS DFASDF ASD', '', '', 0, '2016-06-26 16:19:33', '0000-00-00 00:00:00'),
(11, 5, 'Customer', 'Test Message', 'information-technology-general-controls-top-down-audit-approach_3881_.pdf', 'doc11.pdf', 0, '2016-06-29 22:10:30', '0000-00-00 00:00:00'),
(12, 2, 'Taxtick', 'This is test message', '', '', 0, '2016-07-03 07:14:36', '0000-00-00 00:00:00'),
(13, 2, 'Taxtick', 'attach message', '', '', 0, '2016-07-03 07:15:57', '0000-00-00 00:00:00'),
(14, 2, 'Taxtick', 'attch test 002', 'bang_prem.pdf', 'doc14.pdf', 0, '2016-07-03 07:17:23', '0000-00-00 00:00:00'),
(15, 3, 'Customer', '', '', '', 0, '2016-07-03 07:24:54', '0000-00-00 00:00:00'),
(16, 3, 'Taxtick', '', '', '', 0, '2016-07-03 07:26:20', '0000-00-00 00:00:00'),
(17, 3, 'Taxtick', 'replay test', '', '', 1, '2016-07-03 07:32:34', '0000-00-00 00:00:00'),
(18, 6, 'Customer', 'Hai', 'CA_2.jpg', 'doc18.jpg', 0, '2016-07-04 03:35:22', '0000-00-00 00:00:00'),
(19, 6, 'Customer', '1.\r\n2.\r\n3.\r\n4.\r\n', '', '', 0, '2016-07-04 03:36:00', '0000-00-00 00:00:00'),
(20, 6, 'Taxtick', 'Hello', '', '', 1, '2016-07-04 03:46:50', '0000-00-00 00:00:00'),
(21, 6, 'Customer', '1.\r\n2.\r\n3.\r\n4.\r\n', '', '', 0, '2016-07-04 07:11:06', '0000-00-00 00:00:00'),
(22, 6, 'Taxtick', 'abc', 'Form-16 (Part B).pdf', 'doc22.pdf', 1, '2016-07-05 05:53:49', '0000-00-00 00:00:00'),
(23, 3, 'Customer', '1\r\n2\r\n3', '', '', 0, '2016-07-06 13:32:00', '0000-00-00 00:00:00'),
(24, 5, 'Taxtick', 'test', '', '', 1, '2016-07-07 03:55:51', '0000-00-00 00:00:00'),
(25, 5, 'Taxtick', '1.\r\n2.\r\n3', '', '', 1, '2016-07-07 04:10:10', '0000-00-00 00:00:00'),
(26, 7, 'Customer', '', '', '', 0, '2016-07-07 23:11:23', '0000-00-00 00:00:00'),
(27, 8, 'Customer', 'This is test', '', '', 0, '2016-07-08 00:02:49', '0000-00-00 00:00:00'),
(28, 9, 'Customer', 'mlkbk', '', '', 0, '2016-07-08 00:39:13', '0000-00-00 00:00:00'),
(29, 10, 'Customer', 'ubkjn', '', '', 0, '2016-07-08 00:39:16', '0000-00-00 00:00:00'),
(30, 11, 'Customer', '1\r\n2\r\n4', '', '', 0, '2016-07-08 01:08:55', '0000-00-00 00:00:00'),
(31, 12, 'Customer', 'sfasdfsadf', '', '', 0, '2016-07-08 13:42:00', '0000-00-00 00:00:00'),
(32, 12, 'Customer', '1.\r\n2.\r\n3.', '', '', 0, '2016-07-10 11:20:56', '0000-00-00 00:00:00'),
(33, 12, 'Customer', 'oio', '', '', 0, '2016-07-11 18:20:56', '0000-00-00 00:00:00'),
(34, 13, 'Customer', '1.\r\n2.\r\n3.\r\n4.\r\n', 'Form-16 (Part B).pdf', 'doc34.pdf', 0, '2016-07-12 18:28:48', '0000-00-00 00:00:00'),
(35, 0, 'Customer', '', '', '', 0, '2016-07-12 18:29:11', '0000-00-00 00:00:00'),
(36, 13, 'Taxtick', 'replied', 'Form-16 (Part A).pdf', 'doc36.pdf', 1, '2016-07-12 18:45:44', '0000-00-00 00:00:00'),
(37, 10, 'Customer', 'hai', '', '', 0, '2016-07-12 18:46:22', '0000-00-00 00:00:00'),
(38, 12, 'Customer', 'kllk', '', '', 0, '2016-07-12 20:13:10', '0000-00-00 00:00:00'),
(39, 12, 'Taxtick', 'Email test replay', '', '', 1, '2016-07-15 11:00:42', '0000-00-00 00:00:00'),
(40, 14, 'Customer', 'Ths is email test', '', '', 0, '2016-07-15 11:42:14', '0000-00-00 00:00:00'),
(41, 3, 'Taxtick', 'This is test', '', '', 1, '2016-07-17 15:27:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE IF NOT EXISTS `tbl_customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `reset_token` varchar(100) NOT NULL,
  `reset_expairy` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_customers`
--

INSERT INTO `tbl_customers` (`customer_id`, `email`, `password`, `first_name`, `last_name`, `mobile`, `is_active`, `created_on`, `reset_token`, `reset_expairy`) VALUES
(1, 'sastrylal@gmail.com', 'Admin@123', 'L B Sastry', 'CH', '9290266674', 1, '2016-06-04 22:08:26', 'b992e5f145087d8f6897858d8a705ba2', '2016-07-19 00:54:06'),
(2, 'nagunuriravi@gmail.com', 'kinnu2011', '', '', '8142891889', 1, '2016-06-14 06:04:09', 'd5e26eb3e53d9f9d542dc3ac3a207fa4', '2016-07-01 22:34:09'),
(3, 'saran@chaireturn.com', 'sarankumar', '', '', '9640208282', 1, '2016-06-17 23:22:20', '', '0000-00-00 00:00:00'),
(4, 'y.n.swamy567@gmail.com', 'admin123', '', '', '9052643432', 1, '2016-06-21 00:54:16', '', '0000-00-00 00:00:00'),
(5, 'saikrishnaa@chaireturn.com', '12345', '', '', '7036155666', 1, '2016-06-22 06:14:33', '', '0000-00-00 00:00:00'),
(6, 'sastrylal+21@gmail.com', 'Admin@123', '', '', '9290266674', 1, '2016-06-26 03:20:52', '', '0000-00-00 00:00:00'),
(7, 'sastrylal+21@gmail.com', 'Admin@123', '', '', '9290266674', 1, '2016-06-26 03:22:21', '', '0000-00-00 00:00:00'),
(8, 'sastrylal+22@gmail.com', 'Admin@123', '', '', '9290266674', 1, '2016-06-26 03:26:21', '', '0000-00-00 00:00:00'),
(9, 'sastrylal+22@gmail.com', 'Admin@123', '', '', '9290266674', 1, '2016-06-26 07:19:19', '', '0000-00-00 00:00:00'),
(10, 'saikrishnaa@sbsandco.com', '123', 'sai', 'arumalla', '123', 1, '2016-07-04 02:28:10', 'aecb76a4c86be4830e9c4b3bf92f3a6f', '2016-07-07 19:45:37'),
(11, 'saran@sbsandco.com', 'sarankumar', '', '', '9640208282', 1, '2016-07-04 06:53:58', '', '0000-00-00 00:00:00'),
(12, 'sastrylal+20@gmail.com', 'admin', '', '', '9290266674', 1, '2016-07-05 20:47:33', '', '0000-00-00 00:00:00'),
(13, 'sastrylal+23@gmail.com', 'Admin@123', '', '', '9290266674', 1, '2016-07-07 03:53:38', '', '0000-00-00 00:00:00'),
(14, 'sastrylal+40@gmail.com', 'Admin@123', 'L B Sastry', 'CH', '9290266674', 1, '2016-07-14 20:54:37', 'e2ecfded6f60e75f4c304d0459a57b42', '2016-07-18 09:52:20'),
(15, 'sastrylal+41@gmail.com', 'Admin@123', '', '', '9290266674', 1, '2016-07-14 21:06:30', '', '0000-00-00 00:00:00'),
(16, 'sastrylal+42@gmail.com', 'Admin@123', 'sastrylal+42', '', '9290266674', 1, '2016-07-14 21:08:29', '', '0000-00-00 00:00:00'),
(17, 'sastrylal+43@gmail.com', 'Admin@123', 'sastrylal+43', '', '9290266674', 1, '2016-07-14 21:13:16', '', '0000-00-00 00:00:00'),
(18, 'sastrylal+440@gmail.com', '', 'sastrylal+440', '', '', 1, '2016-07-15 12:06:35', '', '0000-00-00 00:00:00'),
(19, 'sastrylal+44@gmail.com', 'Admin@123', 'sastrylal+44', '', '9290266674', 1, '2016-07-15 12:08:50', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doc_types`
--

CREATE TABLE IF NOT EXISTS `tbl_doc_types` (
  `doc_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_type_name` varchar(100) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`doc_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_doc_types`
--

INSERT INTO `tbl_doc_types` (`doc_type_id`, `doc_type_name`, `is_active`) VALUES
(1, 'Form 16/16A', 1),
(2, 'Form 26AS', 1),
(3, 'Income Tax Return Acknowledgement (ITR-V)', 1),
(4, 'Bank Statements', 1),
(5, 'Pay Slips', 1),
(6, 'Tax Saving Documents', 1),
(7, 'Housing Loan Tax Certificates', 1),
(8, 'Income Tax notices', 1),
(9, 'Others', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_drive`
--

CREATE TABLE IF NOT EXISTS `tbl_drive` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `doc_type` varchar(20) NOT NULL,
  `financial_year` varchar(20) NOT NULL,
  `doc_name` varchar(200) NOT NULL,
  `doc_password` varchar(100) NOT NULL,
  `doc_path` varchar(200) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_drive`
--

INSERT INTO `tbl_drive` (`doc_id`, `customer_id`, `doc_type`, `financial_year`, `doc_name`, `doc_password`, `doc_path`, `created_on`) VALUES
(2, 5, '', '2015-16', 'icai', '', 'doc2.pdf', '2016-06-22 06:21:30'),
(3, 1, 'ITR-V', '2000-2001', 'del test12', 'test12', 'doc3.jpg', '2016-06-29 12:04:28'),
(4, 10, '', '2015-2016', 'ABC', '', 'doc4.jpg', '2016-07-04 03:33:40'),
(5, 10, 'ITR-V', '2005-2006', 'ABC', '', 'doc5.jpg', '2016-07-04 03:34:16'),
(6, 11, 'Form-16', '2015-2016', 'sadf', '', 'doc6.pdf', '2016-07-04 07:05:26'),
(8, 1, 'Form-16', '2001-2002', 'sasd', '', 'doc8.txt', '2016-07-08 00:05:24'),
(11, 1, '5', '2000-2001', 'test', 'no', 'doc11.pdf', '2016-07-13 09:28:13'),
(12, 10, '3', '2000-2001', 'sai', '', 'doc12.xlsx', '2016-07-18 17:48:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faqs`
--

CREATE TABLE IF NOT EXISTS `tbl_faqs` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_faqs`
--

INSERT INTO `tbl_faqs` (`faq_id`, `title`, `description`, `created_on`, `is_active`) VALUES
(1, 'Question001', '<blockquote>\r\n<p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo c</strong>onsequat.</p>\r\n</blockquote>\r\n\r\n<p>oTher tstrd&nbsp; asdf asdf asdfas asdf asdfasdf asdf</p>\r\n', '2016-06-14 11:01:08', 1),
(2, 'Question002', '<p><em>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</em></p>\r\n\r\n<ul>\r\n	<li><em>ttttwerwer</em></li>\r\n	<li><em>werwerwer</em></li>\r\n	<li><em>werwerwe</em></li>\r\n</ul>\r\n', '2016-06-14 11:01:17', 1),
(3, 'Question003', '<p><em><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</strong></em></p>\r\n\r\n<ul>\r\n	<li><strong>123</strong></li>\r\n	<li><strong>2342342</strong></li>\r\n	<li><strong>23423424</strong></li>\r\n</ul>\r\n', '2016-06-14 11:01:26', 1),
(4, 'Question004', '<p><em><strong>Hello, This is a dummy FAQ</strong></em></p>\r\n', '2016-06-14 11:01:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_logs`
--

CREATE TABLE IF NOT EXISTS `tbl_payment_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `slot_id` int(11) NOT NULL,
  `tnx_id` varchar(100) NOT NULL,
  `response` text NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_payment_logs`
--

INSERT INTO `tbl_payment_logs` (`log_id`, `slot_id`, `tnx_id`, `response`) VALUES
(1, 1, '1cf3af28ec6d248c53a9', 'a:46:{s:8:"mihpayid";s:9:"108089206";s:4:"mode";s:0:"";s:6:"status";s:7:"failure";s:14:"unmappedstatus";s:13:"userCancelled";s:3:"key";s:8:"IjyFDW0M";s:5:"txnid";s:20:"1cf3af28ec6d248c53a9";s:6:"amount";s:3:"5.0";s:7:"addedon";s:19:"2016-06-13 02:54:34";s:11:"productinfo";s:12:"Slot Booking";s:9:"firstname";s:10:"L B Sastry";s:8:"lastname";s:0:"";s:8:"address1";s:0:"";s:8:"address2";s:0:"";s:4:"city";s:0:"";s:5:"state";s:0:"";s:7:"country";s:0:"";s:7:"zipcode";s:0:"";s:5:"email";s:19:"sastrylal@gmail.com";s:5:"phone";s:10:"9290266674";s:4:"udf1";s:1:"1";s:4:"udf2";s:0:"";s:4:"udf3";s:0:"";s:4:"udf4";s:0:"";s:4:"udf5";s:0:"";s:4:"udf6";s:0:"";s:4:"udf7";s:0:"";s:4:"udf8";s:0:"";s:4:"udf9";s:0:"";s:5:"udf10";s:0:"";s:4:"hash";s:128:"76ec4272423471ac27db098669d3fc1a89363ed5998bb12ca2c10bb1609dbf21c4e2893bd1a7c88b52ca42e0dd2d75f48ae6812023e6c96c13b028156de14a71";s:6:"field1";s:0:"";s:6:"field2";s:0:"";s:6:"field3";s:0:"";s:6:"field4";s:0:"";s:6:"field5";s:0:"";s:6:"field6";s:0:"";s:6:"field7";s:0:"";s:6:"field8";s:0:"";s:6:"field9";s:17:"Cancelled by user";s:7:"PG_TYPE";s:5:"PAISA";s:12:"bank_ref_num";s:9:"108089206";s:8:"bankcode";s:5:"PAYUW";s:5:"error";s:4:"E000";s:13:"error_Message";s:8:"No Error";s:12:"amount_split";s:2:"{}";s:11:"payuMoneyId";s:9:"108089206";}'),
(2, 4, '133e72bdf49eab11738b', 'a:46:{s:8:"mihpayid";s:9:"108710274";s:4:"mode";s:0:"";s:6:"status";s:7:"failure";s:14:"unmappedstatus";s:13:"userCancelled";s:3:"key";s:8:"IjyFDW0M";s:5:"txnid";s:20:"133e72bdf49eab11738b";s:6:"amount";s:4:"90.0";s:7:"addedon";s:19:"2016-06-18 12:24:34";s:11:"productinfo";s:12:"Slot Booking";s:9:"firstname";s:5:"Saran";s:8:"lastname";s:0:"";s:8:"address1";s:0:"";s:8:"address2";s:0:"";s:4:"city";s:0:"";s:5:"state";s:0:"";s:7:"country";s:0:"";s:7:"zipcode";s:0:"";s:5:"email";s:20:"saran@chaireturn.com";s:5:"phone";s:10:"9640208282";s:4:"udf1";s:1:"4";s:4:"udf2";s:0:"";s:4:"udf3";s:0:"";s:4:"udf4";s:0:"";s:4:"udf5";s:0:"";s:4:"udf6";s:0:"";s:4:"udf7";s:0:"";s:4:"udf8";s:0:"";s:4:"udf9";s:0:"";s:5:"udf10";s:0:"";s:4:"hash";s:128:"785fc939af24a622a689473244788f10b0858f381583f4d6ca7c4488f52b827e78a86c3d2d74b9b05d56061308b2da0bff0800118ddfc9638873fa6a51d67b32";s:6:"field1";s:0:"";s:6:"field2";s:0:"";s:6:"field3";s:0:"";s:6:"field4";s:0:"";s:6:"field5";s:0:"";s:6:"field6";s:0:"";s:6:"field7";s:0:"";s:6:"field8";s:0:"";s:6:"field9";s:17:"Cancelled by user";s:7:"PG_TYPE";s:5:"PAISA";s:12:"bank_ref_num";s:9:"108710274";s:8:"bankcode";s:5:"PAYUW";s:5:"error";s:4:"E000";s:13:"error_Message";s:8:"No Error";s:12:"amount_split";s:2:"{}";s:11:"payuMoneyId";s:9:"108710274";}'),
(3, 7, '6f68c92ea4c1555b33bd', 'a:46:{s:8:"mihpayid";s:9:"110010225";s:4:"mode";s:0:"";s:6:"status";s:7:"failure";s:14:"unmappedstatus";s:13:"userCancelled";s:3:"key";s:8:"IjyFDW0M";s:5:"txnid";s:20:"6f68c92ea4c1555b33bd";s:6:"amount";s:5:"100.0";s:7:"addedon";s:19:"2016-06-30 05:35:56";s:11:"productinfo";s:12:"Slot Booking";s:9:"firstname";s:10:"L B Sastry";s:8:"lastname";s:0:"";s:8:"address1";s:0:"";s:8:"address2";s:0:"";s:4:"city";s:0:"";s:5:"state";s:0:"";s:7:"country";s:0:"";s:7:"zipcode";s:0:"";s:5:"email";s:19:"sastrylal@gmail.com";s:5:"phone";s:10:"9290266674";s:4:"udf1";s:1:"7";s:4:"udf2";s:0:"";s:4:"udf3";s:0:"";s:4:"udf4";s:0:"";s:4:"udf5";s:0:"";s:4:"udf6";s:0:"";s:4:"udf7";s:0:"";s:4:"udf8";s:0:"";s:4:"udf9";s:0:"";s:5:"udf10";s:0:"";s:4:"hash";s:128:"6d0124be9c63b799e2b300a731662e12460535e9d02e202187f06d13b8822df58f4adb47535c1a857e76b02ed699cf708ba491517d5d36bc490014e95829ad39";s:6:"field1";s:0:"";s:6:"field2";s:0:"";s:6:"field3";s:0:"";s:6:"field4";s:0:"";s:6:"field5";s:0:"";s:6:"field6";s:0:"";s:6:"field7";s:0:"";s:6:"field8";s:0:"";s:6:"field9";s:17:"Cancelled by user";s:7:"PG_TYPE";s:5:"PAISA";s:12:"bank_ref_num";s:9:"110010225";s:8:"bankcode";s:5:"PAYUW";s:5:"error";s:4:"E000";s:13:"error_Message";s:8:"No Error";s:12:"amount_split";s:2:"{}";s:11:"payuMoneyId";s:9:"110010225";}'),
(4, 8, '719f0e80bc0d6ec003bf', 'a:49:{s:8:"mihpayid";s:10:"5825976493";s:4:"mode";s:2:"NB";s:6:"status";s:7:"success";s:14:"unmappedstatus";s:8:"captured";s:3:"key";s:8:"IjyFDW0M";s:5:"txnid";s:20:"719f0e80bc0d6ec003bf";s:6:"amount";s:3:"5.0";s:7:"addedon";s:19:"2016-07-04 16:30:51";s:11:"productinfo";s:12:"Slot Booking";s:9:"firstname";s:17:"sai krishna reddy";s:8:"lastname";s:0:"";s:8:"address1";s:0:"";s:8:"address2";s:0:"";s:4:"city";s:0:"";s:5:"state";s:0:"";s:7:"country";s:0:"";s:7:"zipcode";s:0:"";s:5:"email";s:24:"saikrishnaa@sbsandco.com";s:5:"phone";s:10:"7036155666";s:4:"udf1";s:1:"8";s:4:"udf2";s:0:"";s:4:"udf3";s:0:"";s:4:"udf4";s:0:"";s:4:"udf5";s:0:"";s:4:"udf6";s:0:"";s:4:"udf7";s:0:"";s:4:"udf8";s:0:"";s:4:"udf9";s:0:"";s:5:"udf10";s:0:"";s:4:"hash";s:128:"3d9fc7110367d510c2b4b7aebc5aa3b65fc838909c208914d9df800ad7d78f5b8203aff335cd51b5479501380532c1676c467f8874b435553a9afd58b122b7af";s:6:"field1";s:0:"";s:6:"field2";s:0:"";s:6:"field3";s:13:"0110008021811";s:6:"field4";s:0:"";s:6:"field5";s:0:"";s:6:"field6";s:0:"";s:6:"field7";s:0:"";s:6:"field8";s:0:"";s:6:"field9";s:34:"Transaction Completed Successfully";s:7:"PG_TYPE";s:5:"ADBNB";s:18:"encryptedPaymentId";s:32:"A55400F0DF46F85DC66F2C8C48AF58DD";s:12:"bank_ref_num";s:10:"0008021811";s:8:"bankcode";s:4:"ADBB";s:5:"error";s:4:"E000";s:13:"error_Message";s:8:"No Error";s:12:"amount_split";s:14:"{"PAYU":"5.0"}";s:11:"payuMoneyId";s:9:"110539188";s:8:"discount";s:4:"0.00";s:16:"net_amount_debit";s:1:"5";}'),
(5, 12, 'b3189dc7fa510960ac99', 'a:49:{s:8:"mihpayid";s:10:"5828111245";s:4:"mode";s:2:"NB";s:6:"status";s:7:"success";s:14:"unmappedstatus";s:8:"captured";s:3:"key";s:8:"IjyFDW0M";s:5:"txnid";s:20:"b3189dc7fa510960ac99";s:6:"amount";s:3:"1.0";s:7:"addedon";s:19:"2016-07-07 17:35:24";s:11:"productinfo";s:12:"Slot Booking";s:9:"firstname";s:17:"sai krishna reddy";s:8:"lastname";s:0:"";s:8:"address1";s:0:"";s:8:"address2";s:0:"";s:4:"city";s:0:"";s:5:"state";s:0:"";s:7:"country";s:0:"";s:7:"zipcode";s:0:"";s:5:"email";s:24:"saikrishnaa@sbsandco.com";s:5:"phone";s:10:"7036155666";s:4:"udf1";s:2:"12";s:4:"udf2";s:0:"";s:4:"udf3";s:0:"";s:4:"udf4";s:0:"";s:4:"udf5";s:0:"";s:4:"udf6";s:0:"";s:4:"udf7";s:0:"";s:4:"udf8";s:0:"";s:4:"udf9";s:0:"";s:5:"udf10";s:0:"";s:4:"hash";s:128:"e27a3cc6c8cbb18a8d5becd01c4735d91411676161c6e523448e715f6f45ec63c4aa88287bf1688d87a4782fcf80b53c3dad5a2b572a8bf0b329152213b19a5c";s:6:"field1";s:0:"";s:6:"field2";s:0:"";s:6:"field3";s:13:"0110008058163";s:6:"field4";s:0:"";s:6:"field5";s:0:"";s:6:"field6";s:0:"";s:6:"field7";s:0:"";s:6:"field8";s:0:"";s:6:"field9";s:34:"Transaction Completed Successfully";s:7:"PG_TYPE";s:5:"ADBNB";s:18:"encryptedPaymentId";s:32:"0995A48666A02C80D5198C8F6F294874";s:12:"bank_ref_num";s:10:"0008058163";s:8:"bankcode";s:4:"ADBB";s:5:"error";s:4:"E000";s:13:"error_Message";s:8:"No Error";s:12:"amount_split";s:14:"{"PAYU":"1.0"}";s:11:"payuMoneyId";s:9:"110916215";s:8:"discount";s:4:"0.00";s:16:"net_amount_debit";s:1:"1";}'),
(6, 13, '13bdb9bd874ee0891170', 'a:46:{s:8:"mihpayid";s:9:"110977618";s:4:"mode";s:0:"";s:6:"status";s:7:"failure";s:14:"unmappedstatus";s:13:"userCancelled";s:3:"key";s:8:"IjyFDW0M";s:5:"txnid";s:20:"13bdb9bd874ee0891170";s:6:"amount";s:5:"100.0";s:7:"addedon";s:19:"2016-07-08 11:13:47";s:11:"productinfo";s:12:"Slot Booking";s:9:"firstname";s:10:"L B Sastry";s:8:"lastname";s:0:"";s:8:"address1";s:0:"";s:8:"address2";s:0:"";s:4:"city";s:0:"";s:5:"state";s:0:"";s:7:"country";s:0:"";s:7:"zipcode";s:0:"";s:5:"email";s:19:"sastrylal@gmail.com";s:5:"phone";s:10:"9290266674";s:4:"udf1";s:2:"13";s:4:"udf2";s:0:"";s:4:"udf3";s:0:"";s:4:"udf4";s:0:"";s:4:"udf5";s:0:"";s:4:"udf6";s:0:"";s:4:"udf7";s:0:"";s:4:"udf8";s:0:"";s:4:"udf9";s:0:"";s:5:"udf10";s:0:"";s:4:"hash";s:128:"d5ca87cf9de01045b02c08e165891684c1b716af646f2a4fc64a6b3508fe02804abd95f79a8bcf1a0063b2458692c07ccc904e8edb205d458f5183120f63914b";s:6:"field1";s:0:"";s:6:"field2";s:0:"";s:6:"field3";s:0:"";s:6:"field4";s:0:"";s:6:"field5";s:0:"";s:6:"field6";s:0:"";s:6:"field7";s:0:"";s:6:"field8";s:0:"";s:6:"field9";s:17:"Cancelled by user";s:7:"PG_TYPE";s:5:"PAISA";s:12:"bank_ref_num";s:9:"110977618";s:8:"bankcode";s:5:"PAYUW";s:5:"error";s:4:"E000";s:13:"error_Message";s:8:"No Error";s:12:"amount_split";s:2:"{}";s:11:"payuMoneyId";s:9:"110977618";}'),
(7, 18, '0455c5eb21a7f5aafa2f', 'a:46:{s:8:"mihpayid";s:9:"111003343";s:4:"mode";s:0:"";s:6:"status";s:7:"failure";s:14:"unmappedstatus";s:13:"userCancelled";s:3:"key";s:8:"IjyFDW0M";s:5:"txnid";s:20:"0455c5eb21a7f5aafa2f";s:6:"amount";s:5:"100.0";s:7:"addedon";s:19:"2016-07-08 14:01:24";s:11:"productinfo";s:12:"Slot Booking";s:9:"firstname";s:17:"sai krishna reddy";s:8:"lastname";s:0:"";s:8:"address1";s:0:"";s:8:"address2";s:0:"";s:4:"city";s:0:"";s:5:"state";s:0:"";s:7:"country";s:0:"";s:7:"zipcode";s:0:"";s:5:"email";s:24:"saikrishnaa@sbsandco.com";s:5:"phone";s:10:"7036155666";s:4:"udf1";s:2:"18";s:4:"udf2";s:0:"";s:4:"udf3";s:0:"";s:4:"udf4";s:0:"";s:4:"udf5";s:0:"";s:4:"udf6";s:0:"";s:4:"udf7";s:0:"";s:4:"udf8";s:0:"";s:4:"udf9";s:0:"";s:5:"udf10";s:0:"";s:4:"hash";s:128:"b8db9e2d932a952f4e4899ac4235a1648c9ac49aca15364ed24fb4ccd6d0b00f596cb001bf69c43eaee1109e845b542da383e2c88327f42de9e41ed69d157cbc";s:6:"field1";s:0:"";s:6:"field2";s:0:"";s:6:"field3";s:0:"";s:6:"field4";s:0:"";s:6:"field5";s:0:"";s:6:"field6";s:0:"";s:6:"field7";s:0:"";s:6:"field8";s:0:"";s:6:"field9";s:17:"Cancelled by user";s:7:"PG_TYPE";s:5:"PAISA";s:12:"bank_ref_num";s:9:"111003343";s:8:"bankcode";s:5:"PAYUW";s:5:"error";s:4:"E000";s:13:"error_Message";s:8:"No Error";s:12:"amount_split";s:2:"{}";s:11:"payuMoneyId";s:9:"111003343";}'),
(8, 23, '2a5112089237a6d119cd', 'a:46:{s:8:"mihpayid";s:9:"111536955";s:4:"mode";s:0:"";s:6:"status";s:7:"failure";s:14:"unmappedstatus";s:13:"userCancelled";s:3:"key";s:8:"IjyFDW0M";s:5:"txnid";s:20:"2a5112089237a6d119cd";s:6:"amount";s:5:"100.0";s:7:"addedon";s:19:"2016-07-13 13:17:21";s:11:"productinfo";s:12:"Slot Booking";s:9:"firstname";s:10:"L B Sastry";s:8:"lastname";s:0:"";s:8:"address1";s:0:"";s:8:"address2";s:0:"";s:4:"city";s:0:"";s:5:"state";s:0:"";s:7:"country";s:0:"";s:7:"zipcode";s:0:"";s:5:"email";s:19:"sastrylal@gmail.com";s:5:"phone";s:10:"9290266674";s:4:"udf1";s:2:"23";s:4:"udf2";s:0:"";s:4:"udf3";s:0:"";s:4:"udf4";s:0:"";s:4:"udf5";s:0:"";s:4:"udf6";s:0:"";s:4:"udf7";s:0:"";s:4:"udf8";s:0:"";s:4:"udf9";s:0:"";s:5:"udf10";s:0:"";s:4:"hash";s:128:"a157f1867cf734ebc02cd6ec15df512c9c4f4c2c36f1d6f7ad21b90bf8ddb4a4cd61a3d0280224f507010a335115523ca7750cf07f1c2cc7091f9bb2f9e52e9b";s:6:"field1";s:0:"";s:6:"field2";s:0:"";s:6:"field3";s:0:"";s:6:"field4";s:0:"";s:6:"field5";s:0:"";s:6:"field6";s:0:"";s:6:"field7";s:0:"";s:6:"field8";s:0:"";s:6:"field9";s:17:"Cancelled by user";s:7:"PG_TYPE";s:5:"PAISA";s:12:"bank_ref_num";s:9:"111536955";s:8:"bankcode";s:5:"PAYUW";s:5:"error";s:4:"E000";s:13:"error_Message";s:8:"No Error";s:12:"amount_split";s:2:"{}";s:11:"payuMoneyId";s:9:"111536955";}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_promos`
--

CREATE TABLE IF NOT EXISTS `tbl_promos` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_code` varchar(100) NOT NULL,
  `promo_type` enum('Flat','Percentage') NOT NULL DEFAULT 'Flat',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_on` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_promos`
--

INSERT INTO `tbl_promos` (`promo_id`, `promo_code`, `promo_type`, `discount`, `created_on`, `is_active`) VALUES
(2, 'ABC', 'Flat', '10.00', '2016-06-17 11:40:00', 1),
(3, 'ABCD', 'Percentage', '20.00', '2016-06-17 11:40:17', 1),
(4, 'ABC123', 'Flat', '95.00', '2016-07-04 03:24:02', 0),
(5, 'ABCDE', 'Percentage', '10.00', '2016-07-05 06:00:06', 1),
(6, '999', 'Flat', '99.00', '2016-07-07 05:03:04', 1),
(7, 'FreeTT', 'Flat', '100.00', '2016-07-08 13:54:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE IF NOT EXISTS `tbl_roles` (
  `role_id` varchar(20) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`role_id`, `role_name`) VALUES
('ADMIN', 'Administrator'),
('STAFF', 'Staff'),
('SUPER_ADMIN', 'Superadmin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slots`
--

CREATE TABLE IF NOT EXISTS `tbl_slots` (
  `slot_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `purpose` varchar(100) NOT NULL,
  `slot_date` datetime NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `sharing_software` varchar(50) NOT NULL,
  `promo_code` varchar(50) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `txn_id` varchar(100) NOT NULL,
  `payment_status` enum('Paid','Not Paid') NOT NULL DEFAULT 'Not Paid',
  `slot_status` enum('In Process','Completed') DEFAULT NULL,
  `reschedule` varchar(255) NOT NULL,
  `assign_to` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`slot_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `tbl_slots`
--

INSERT INTO `tbl_slots` (`slot_id`, `customer_id`, `purpose`, `slot_date`, `customer_name`, `email`, `mobile`, `sharing_software`, `promo_code`, `amount`, `txn_id`, `payment_status`, `slot_status`, `reschedule`, `assign_to`, `created_on`) VALUES
(29, 1, 'Income Tax Return e-Filing', '2016-07-28 10:30:00', 'L B Sastry', 'sastrylal@gmail.com', '9290266674', 'Skype', 'FREETT', '0.00', '', 'Paid', 'Completed', '', 1, '2016-07-16 00:42:56'),
(30, 1, 'Income Tax Return e-Filing', '2016-07-16 13:30:00', 'Slot11:00AM', 'sastrylal@gmail.com', '9290266674', 'Skype', 'FREETT', '0.00', '', 'Paid', 'In Process', '16/07/2016 11:00:00<br/>18/07/2016 14:00:00<br/>16/07/2016 13:30:00<br/>', 4, '2016-07-16 10:21:20'),
(31, 1, 'Income Tax Return e-Filing', '2016-07-16 11:30:00', 'Slot11:30AM', 'sastrylal@gmail.com', '9290266674', 'Skype', 'FREETT', '0.00', '', 'Paid', 'Completed', '', 2, '2016-07-16 10:21:46'),
(32, 1, 'Income Tax Return e-Filing', '2016-07-16 12:00:00', 'Slot12:00AM', 'sastrylal@gmail.com', '9290266674', 'Skype', 'FREETT', '0.00', '', 'Paid', 'In Process', '', 1, '2016-07-16 10:22:30'),
(33, 1, 'Income Tax Return e-Filing', '2016-07-16 12:30:00', 'Slot12:30AM', 'sastrylal@gmail.com', '9290266674', 'Skype', 'FREETT', '0.00', '', 'Paid', 'Completed', '', 4, '2016-07-16 10:22:54'),
(34, 10, 'Income Tax Return e-Filing', '2016-08-01 10:00:00', 'sai', 'saikrishnaa@sbsandco.com', '7036155666', 'Teamviewer', 'FreeTT', '0.00', '', 'Paid', 'In Process', '', 2, '2016-07-18 17:41:06'),
(35, 10, 'Income Tax Return e-Filing', '2016-07-18 10:30:00', 'sai krishna reddy arumalla', 'saikrishnaa@sbsandco.com', '7036155666', 'Skype', 'FreeTT', '0.00', '', 'Paid', 'Completed', '18/07/2016 10:30:00<br/>', 1, '2016-07-18 17:42:10'),
(36, 1, 'Income Tax Return e-Filing', '2016-07-26 11:30:00', 'L B Sastry', 'sastrylal@gmail.com', '9290266674', 'Phone', 'FREETT', '0.00', '', 'Paid', 'In Process', '', 0, '2016-07-21 22:37:50'),
(37, 1, 'Income Tax Return e-Filing', '2016-07-22 13:00:00', 'L B Sastry', 'sastrylal@gmail.com', '9290266674', 'Skype', 'FREETT', '100.00', 'df390c89b8b0c5aac35e', 'Not Paid', NULL, '', 0, '2016-07-22 10:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slots_available`
--

CREATE TABLE IF NOT EXISTS `tbl_slots_available` (
  `slot_id` int(11) NOT NULL AUTO_INCREMENT,
  `slot_date` datetime NOT NULL,
  `slot_max` tinyint(4) NOT NULL DEFAULT '0',
  `slot_booking_cnt` tinyint(4) NOT NULL DEFAULT '0',
  `slot_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`slot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) NOT NULL DEFAULT '',
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `role_id`, `username`, `password`, `email`, `first_name`, `last_name`, `is_active`) VALUES
(1, 'ADMIN', 'admin', 'admin', 'admin@gmail.com', 'admin', 'ch', 1),
(2, 'STAFF', 'user01@gmail.com', 'Admin@123', 'user01@gmail.com', 'user01', 'ch1', 1),
(4, 'STAFF', 'saikrishnaa@chaireturn.com', '123', 'saikrishnaa@chaireturn.com', 'Sai', 'chaireturn', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
